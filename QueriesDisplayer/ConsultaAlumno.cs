﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;


namespace QueriesDisplayer
{
    public partial class ConsultaAlumno : Form
    {
        // Metodo para obtener las posiciones de las esquinas del windows form
        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        private static extern IntPtr CreateRoundRectRgn
        (
            int nLeftRect,     // x-coordinate of upper-left corner
            int nTopRect,      // y-coordinate of upper-left corner
            int nRightRect,    // x-coordinate of lower-right corner
            int nBottomRect,   // y-coordinate of lower-right corner
            int nWidthEllipse, // height of ellipse
            int nHeightEllipse // width of ellipse
        );


        public ConsultaAlumno()
        {
            InitializeComponent();

            // Asignacion de esquinas redondeadas
            this.FormBorderStyle = FormBorderStyle.None;
            Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, Width, Height, 20, 20));

            // Borrado de campos
            InitializeComponent();
            matriculaText.Text = "";
            nombreText.Text = "";
            apellidoPText.Text = "";
            apellidoMText.Text = "";
            seguroText.Text = "";
            carreraText.Text = "";
            telefonosText.Text = "";
            diaText.Text = "";
            mesText.Text = "";
            anoText.Text = "";
            paisNText.Text = "";
            estadoNText.Text = "";
            nacionalidadText.Text = "";
            claveRText.Text = "";
            nombreRText.Text = "";
            direccionRText.Text = "";
            Telefono1Text.Text = "";
            telefono2Text.Text = "";
            mail1Text.Text = "";
            mail2Text.Text = "";


            // Desaparicion de campos
            InitializeComponent();
            nombreText.Visible = false;
            nombreTitle.Visible = false;
            apellidoPText.Visible = false;
            apellidoPTitle.Visible = false;
            apellidoMText.Visible = false;
            apellidoMTitle.Visible = false;
            seguroText.Visible = false;
            seguroTitle.Visible = false;
            carreraText.Visible = false;
            carreraTitle.Visible = false;
            telefonosText.Visible = false;
            telefonosTitle.Visible = false;
            diaText.Visible = false;
            diaTitle.Visible = false;
            mesText.Visible = false;
            mesTitle.Visible = false;
            anoText.Visible = false;
            anoTitle.Visible = false;
            paisNText.Visible = false;
            paisNTitle.Visible = false;
            estadoNText.Visible = false;
            estadoNTitle.Visible = false;
            nacionalidadText.Visible = false;
            nacionalidadTitle.Visible = false;
            claveRText.Visible = false;
            claveRTitle.Visible = false;
            nombreRText.Visible = false;
            nombreRTitle.Visible = false;
            direccionRText.Visible = false;
            direccionRTitle.Visible = false;
            Telefono1Text.Visible = false;
            telefono1Title.Visible = false;
            telefono2Text.Visible = false;
            telefono2Title.Visible = false;
            mail1Text.Visible = false;
            mail1Title.Visible = false;
            mail2Text.Visible = false;
            mail2Title.Visible = false;
        }

        // Funcion encargada de checar que los campos esten completos, es decir, que contengan texto o 
        // datos para ingresar.
        private bool EntradaCompleta()
        {
            if (matriculaText.Text=="")
            {
                return false;
            }
            return true;
        }

        private void regresarBoton_Click(object sender, EventArgs e)
        {
            this.Hide();
            QueriesDisplayer inicio = new QueriesDisplayer();
            inicio.Show();
        }

        private void ejecutarBoton_Click(object sender, EventArgs e)
        {
            if (EntradaCompleta())
            {

            }
            else
            {
                MessageBox.Show("No se pueden dejar entradas vacias", "Bases de datos TEC", MessageBoxButtons.OK);
            }
        }

        private void telefonosText_TextChanged(object sender, EventArgs e)
        {

        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void ConsultaAlumno_Load(object sender, EventArgs e)
        {

        }

        private void label13_Click(object sender, EventArgs e)
        {

        }
    }
}
