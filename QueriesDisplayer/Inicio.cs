﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing.Text;
using System.Runtime.InteropServices;

namespace QueriesDisplayer
{
    
    public partial class QueriesDisplayer : Form
    {
        // Metodo para obtener las posiciones de las esquinas del windows form
        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        private static extern IntPtr CreateRoundRectRgn
        (
            int nLeftRect,     // x-coordinate of upper-left corner
            int nTopRect,      // y-coordinate of upper-left corner
            int nRightRect,    // x-coordinate of lower-right corner
            int nBottomRect,   // y-coordinate of lower-right corner
            int nWidthEllipse, // height of ellipse
            int nHeightEllipse // width of ellipse
        );

        public QueriesDisplayer()
        {
            InitializeComponent();

            // Borrado de Campo
            this.accion.Text = "";
            this.tabla.Text = "";

           // Asignacion de esquinas redondeadas
            this.FormBorderStyle = FormBorderStyle.None;
            Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, Width, Height, 20, 20));

        }

        private void LaberQuerie_Click(object sender, EventArgs e)
        {
        }
        private void ResizeGrid()
        {
            //for(int x = 0; x < Results.Columns.Count; x++)
            //{
            //    Results.Columns[x].DefaultCellStyle.Font = new Font("Century Gothic", 10);
            //    Results.Columns[x].HeaderCell.Style.Font = new Font("Century Gothic", 10);
            //}
        }

        private void Results_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void EleccionQuery_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(accion.Text=="" || accion.Text==null || tabla.Text=="" || tabla.Text == null)
            {
                MessageBox.Show("No se pueden dejar entradas vacias", "Bases de datos TEC", MessageBoxButtons.OK);
            }
            else if (accion.Text == "Alta")
            {
                if (tabla.Text == "Profesor")
                {
                    this.Hide();
                    AltaProfesor altaMaestro = new AltaProfesor();
                    altaMaestro.Show();
                }else if (tabla.Text == "Alumno")
                {
                    this.Hide();
                    AltaAlumno altaAlumno = new AltaAlumno();
                    altaAlumno.Show();
                } else if (tabla.Text == "Materia")
                {
                    this.Hide();
                    AltaMateria altaMateria = new AltaMateria();
                    altaMateria.Show();
                }
                else if (tabla.Text == "Grupos")
                {
                    this.Hide();
                    AltaGrupo altaGrupo = new AltaGrupo();
                    altaGrupo.Show();
                }
            }
            else if (accion.Text == "Baja")
            {

            }
            else if (accion.Text == "Modificación")
            {

            }
            else if (accion.Text == "Consultas")
            {
                if (tabla.Text == "Profesor")
                {
                    
                }
                else if (tabla.Text == "Alumno")
                {
                    this.Hide();
                    ConsultaAlumno consultaAlumano = new ConsultaAlumno();
                    consultaAlumano.Show();

                }
                else if (tabla.Text == "Materia")
                {
                  
                }
                else if (tabla.Text == "Grupos")
                {
                   
                }
            }


            //string query;
            //string connetionString;
            //SqlConnection connection;
            //SqlDataAdapter command;
            //DataTable table;

            //query = "Select * from Users"; //QueryText.Text; // We asign the text in the box input to a string
            //connetionString = "Data Source=ccastro.database.windows.net;Initial Catalog=DataBaseOnline;User ID=carloscastro;Password=Ultimate45$";

            //connection = new SqlConnection(connetionString);
            //try
            //{
            //    connection.Open();
            //    command = new SqlDataAdapter(query, connection);
            //    table = new DataTable();
            //    command.Fill(table);
            //    Results.DataSource = table;
            //    ResizeGrid();


            //    table.Dispose();
            //    command.Dispose();
            //    connection.Close();


            //}
            //catch (System.Data.SqlClient.SqlException ex)
            //{
            //    MessageBox.Show(ex.Message + " !");
            //}
            //catch (InvalidOperationException ex)
            //{
            //    MessageBox.Show("There is no text in the TextBox !");
            //}
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}

