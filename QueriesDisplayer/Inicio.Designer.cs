﻿namespace QueriesDisplayer
{
    partial class QueriesDisplayer
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(QueriesDisplayer));
            this.label1 = new System.Windows.Forms.Label();
            this.LaberQuerie = new System.Windows.Forms.Label();
            this.accion = new System.Windows.Forms.ComboBox();
            this.tabla = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.EmpezarBoton = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // LaberQuerie
            // 
            resources.ApplyResources(this.LaberQuerie, "LaberQuerie");
            this.LaberQuerie.BackColor = System.Drawing.Color.Transparent;
            this.LaberQuerie.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LaberQuerie.Name = "LaberQuerie";
            this.LaberQuerie.Click += new System.EventHandler(this.LaberQuerie_Click);
            // 
            // accion
            // 
            resources.ApplyResources(this.accion, "accion");
            this.accion.FormattingEnabled = true;
            this.accion.Items.AddRange(new object[] {
            resources.GetString("accion.Items"),
            resources.GetString("accion.Items1"),
            resources.GetString("accion.Items2"),
            resources.GetString("accion.Items3")});
            this.accion.Name = "accion";
            this.accion.SelectedIndexChanged += new System.EventHandler(this.EleccionQuery_SelectedIndexChanged);
            // 
            // tabla
            // 
            resources.ApplyResources(this.tabla, "tabla");
            this.tabla.FormattingEnabled = true;
            this.tabla.Items.AddRange(new object[] {
            resources.GetString("tabla.Items"),
            resources.GetString("tabla.Items1"),
            resources.GetString("tabla.Items2"),
            resources.GetString("tabla.Items3")});
            this.tabla.Name = "tabla";
            this.tabla.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.Name = "label2";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label3.Name = "label3";
            // 
            // EmpezarBoton
            // 
            resources.ApplyResources(this.EmpezarBoton, "EmpezarBoton");
            this.EmpezarBoton.Name = "EmpezarBoton";
            this.EmpezarBoton.UseVisualStyleBackColor = true;
            this.EmpezarBoton.Click += new System.EventHandler(this.button1_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(109)))), ((int)(((byte)(188)))));
            resources.ApplyResources(this.pictureBox1, "pictureBox1");
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.TabStop = false;
            // 
            // QueriesDisplayer
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.LaberQuerie);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.EmpezarBoton);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tabla);
            this.Controls.Add(this.accion);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "QueriesDisplayer";
            this.ShowIcon = false;
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label LaberQuerie;
        private System.Windows.Forms.ComboBox accion;
        private System.Windows.Forms.ComboBox tabla;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button EmpezarBoton;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

