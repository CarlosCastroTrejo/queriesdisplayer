﻿namespace QueriesDisplayer
{
    partial class ConsultaAlumno
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConsultaAlumno));
            this.LaberQuerie = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.regresarBoton = new System.Windows.Forms.Button();
            this.ejecutarBoton = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.matriculaText = new System.Windows.Forms.TextBox();
            this.nominaTitle = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.telefonosTitle = new System.Windows.Forms.Label();
            this.mail1Text = new System.Windows.Forms.TextBox();
            this.mail1Title = new System.Windows.Forms.Label();
            this.mail2Text = new System.Windows.Forms.TextBox();
            this.mail2Title = new System.Windows.Forms.Label();
            this.telefono2Text = new System.Windows.Forms.TextBox();
            this.telefono2Title = new System.Windows.Forms.Label();
            this.nombreRText = new System.Windows.Forms.TextBox();
            this.nombreRTitle = new System.Windows.Forms.Label();
            this.direccionRText = new System.Windows.Forms.TextBox();
            this.direccionRTitle = new System.Windows.Forms.Label();
            this.Telefono1Text = new System.Windows.Forms.TextBox();
            this.telefono1Title = new System.Windows.Forms.Label();
            this.claveRText = new System.Windows.Forms.TextBox();
            this.claveRTitle = new System.Windows.Forms.Label();
            this.carreraTitle = new System.Windows.Forms.Label();
            this.seguroText = new System.Windows.Forms.TextBox();
            this.seguroTitle = new System.Windows.Forms.Label();
            this.datosTitle = new System.Windows.Forms.Label();
            this.nacionalidadText = new System.Windows.Forms.TextBox();
            this.nacionalidadTitle = new System.Windows.Forms.Label();
            this.anoText = new System.Windows.Forms.TextBox();
            this.anoTitle = new System.Windows.Forms.Label();
            this.estadoNText = new System.Windows.Forms.TextBox();
            this.estadoNTitle = new System.Windows.Forms.Label();
            this.paisNText = new System.Windows.Forms.TextBox();
            this.paisNTitle = new System.Windows.Forms.Label();
            this.mesText = new System.Windows.Forms.TextBox();
            this.mesTitle = new System.Windows.Forms.Label();
            this.diaText = new System.Windows.Forms.TextBox();
            this.diaTitle = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.apellidoMText = new System.Windows.Forms.TextBox();
            this.apellidoMTitle = new System.Windows.Forms.Label();
            this.apellidoPText = new System.Windows.Forms.TextBox();
            this.apellidoPTitle = new System.Windows.Forms.Label();
            this.nombreText = new System.Windows.Forms.TextBox();
            this.nombreTitle = new System.Windows.Forms.Label();
            this.telefonosText = new System.Windows.Forms.TextBox();
            this.carreraText = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // LaberQuerie
            // 
            this.LaberQuerie.AutoSize = true;
            this.LaberQuerie.BackColor = System.Drawing.Color.Transparent;
            this.LaberQuerie.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LaberQuerie.Font = new System.Drawing.Font("Century Gothic", 24F);
            this.LaberQuerie.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.LaberQuerie.Location = new System.Drawing.Point(317, 30);
            this.LaberQuerie.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LaberQuerie.Name = "LaberQuerie";
            this.LaberQuerie.Size = new System.Drawing.Size(595, 39);
            this.LaberQuerie.TabIndex = 23;
            this.LaberQuerie.Text = "Base de datos TEC - Consulta alumno";
            this.LaberQuerie.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(109)))), ((int)(((byte)(188)))));
            this.pictureBox1.ErrorImage = null;
            this.pictureBox1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1159, 100);
            this.pictureBox1.TabIndex = 24;
            this.pictureBox1.TabStop = false;
            // 
            // regresarBoton
            // 
            this.regresarBoton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(109)))), ((int)(((byte)(188)))));
            this.regresarBoton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.regresarBoton.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.regresarBoton.ForeColor = System.Drawing.Color.White;
            this.regresarBoton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.regresarBoton.Location = new System.Drawing.Point(436, 117);
            this.regresarBoton.Name = "regresarBoton";
            this.regresarBoton.Size = new System.Drawing.Size(148, 41);
            this.regresarBoton.TabIndex = 139;
            this.regresarBoton.Text = "Regresar";
            this.regresarBoton.UseVisualStyleBackColor = false;
            this.regresarBoton.Click += new System.EventHandler(this.regresarBoton_Click);
            // 
            // ejecutarBoton
            // 
            this.ejecutarBoton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(109)))), ((int)(((byte)(188)))));
            this.ejecutarBoton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ejecutarBoton.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ejecutarBoton.ForeColor = System.Drawing.Color.White;
            this.ejecutarBoton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ejecutarBoton.Location = new System.Drawing.Point(264, 117);
            this.ejecutarBoton.Name = "ejecutarBoton";
            this.ejecutarBoton.Size = new System.Drawing.Size(148, 41);
            this.ejecutarBoton.TabIndex = 138;
            this.ejecutarBoton.Text = "Ejecutar";
            this.ejecutarBoton.UseVisualStyleBackColor = false;
            this.ejecutarBoton.Click += new System.EventHandler(this.ejecutarBoton_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label6.Location = new System.Drawing.Point(26, 125);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(189, 23);
            this.label6.TabIndex = 142;
            this.label6.Text = "Información básica";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // matriculaText
            // 
            this.matriculaText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matriculaText.Location = new System.Drawing.Point(131, 166);
            this.matriculaText.Name = "matriculaText";
            this.matriculaText.Size = new System.Drawing.Size(219, 31);
            this.matriculaText.TabIndex = 141;
            // 
            // nominaTitle
            // 
            this.nominaTitle.AutoSize = true;
            this.nominaTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.nominaTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nominaTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.nominaTitle.Location = new System.Drawing.Point(26, 169);
            this.nominaTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.nominaTitle.Name = "nominaTitle";
            this.nominaTitle.Size = new System.Drawing.Size(101, 22);
            this.nominaTitle.TabIndex = 140;
            this.nominaTitle.Text = "Matrícula:";
            this.nominaTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox2.ErrorImage = null;
            this.pictureBox2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pictureBox2.Location = new System.Drawing.Point(0, 97);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(606, 110);
            this.pictureBox2.TabIndex = 143;
            this.pictureBox2.TabStop = false;
            // 
            // telefonosTitle
            // 
            this.telefonosTitle.AutoSize = true;
            this.telefonosTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.telefonosTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.telefonosTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.telefonosTitle.Location = new System.Drawing.Point(767, 244);
            this.telefonosTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.telefonosTitle.Name = "telefonosTitle";
            this.telefonosTitle.Size = new System.Drawing.Size(99, 22);
            this.telefonosTitle.TabIndex = 202;
            this.telefonosTitle.Text = "Teléfonos:";
            this.telefonosTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.telefonosTitle.Visible = false;
            this.telefonosTitle.Click += new System.EventHandler(this.label13_Click);
            // 
            // mail1Text
            // 
            this.mail1Text.Enabled = false;
            this.mail1Text.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mail1Text.Location = new System.Drawing.Point(836, 539);
            this.mail1Text.Name = "mail1Text";
            this.mail1Text.Size = new System.Drawing.Size(244, 31);
            this.mail1Text.TabIndex = 198;
            this.mail1Text.Visible = false;
            // 
            // mail1Title
            // 
            this.mail1Title.AutoSize = true;
            this.mail1Title.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.mail1Title.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mail1Title.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.mail1Title.Location = new System.Drawing.Point(721, 542);
            this.mail1Title.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.mail1Title.Name = "mail1Title";
            this.mail1Title.Size = new System.Drawing.Size(103, 22);
            this.mail1Title.TabIndex = 197;
            this.mail1Title.Text = "1er e-mail:";
            this.mail1Title.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.mail1Title.Visible = false;
            // 
            // mail2Text
            // 
            this.mail2Text.Enabled = false;
            this.mail2Text.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mail2Text.Location = new System.Drawing.Point(728, 594);
            this.mail2Text.Name = "mail2Text";
            this.mail2Text.Size = new System.Drawing.Size(244, 31);
            this.mail2Text.TabIndex = 196;
            this.mail2Text.Visible = false;
            // 
            // mail2Title
            // 
            this.mail2Title.AutoSize = true;
            this.mail2Title.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.mail2Title.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mail2Title.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.mail2Title.Location = new System.Drawing.Point(613, 597);
            this.mail2Title.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.mail2Title.Name = "mail2Title";
            this.mail2Title.Size = new System.Drawing.Size(110, 22);
            this.mail2Title.TabIndex = 195;
            this.mail2Title.Text = "2do e-mail:";
            this.mail2Title.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.mail2Title.Visible = false;
            // 
            // telefono2Text
            // 
            this.telefono2Text.Enabled = false;
            this.telefono2Text.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.telefono2Text.Location = new System.Drawing.Point(582, 648);
            this.telefono2Text.Name = "telefono2Text";
            this.telefono2Text.Size = new System.Drawing.Size(244, 31);
            this.telefono2Text.TabIndex = 194;
            this.telefono2Text.Visible = false;
            // 
            // telefono2Title
            // 
            this.telefono2Title.AutoSize = true;
            this.telefono2Title.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.telefono2Title.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.telefono2Title.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.telefono2Title.Location = new System.Drawing.Point(440, 651);
            this.telefono2Title.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.telefono2Title.Name = "telefono2Title";
            this.telefono2Title.Size = new System.Drawing.Size(132, 22);
            this.telefono2Title.TabIndex = 193;
            this.telefono2Title.Text = "2do teléfono:";
            this.telefono2Title.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.telefono2Title.Visible = false;
            // 
            // nombreRText
            // 
            this.nombreRText.Enabled = false;
            this.nombreRText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nombreRText.Location = new System.Drawing.Point(339, 539);
            this.nombreRText.Name = "nombreRText";
            this.nombreRText.Size = new System.Drawing.Size(360, 31);
            this.nombreRText.TabIndex = 192;
            this.nombreRText.Visible = false;
            // 
            // nombreRTitle
            // 
            this.nombreRTitle.AutoSize = true;
            this.nombreRTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.nombreRTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nombreRTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.nombreRTitle.Location = new System.Drawing.Point(234, 542);
            this.nombreRTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.nombreRTitle.Name = "nombreRTitle";
            this.nombreRTitle.Size = new System.Drawing.Size(89, 22);
            this.nombreRTitle.TabIndex = 191;
            this.nombreRTitle.Text = "Nombre:";
            this.nombreRTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.nombreRTitle.Visible = false;
            // 
            // direccionRText
            // 
            this.direccionRText.Enabled = false;
            this.direccionRText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.direccionRText.Location = new System.Drawing.Point(142, 594);
            this.direccionRText.Name = "direccionRText";
            this.direccionRText.Size = new System.Drawing.Size(440, 31);
            this.direccionRText.TabIndex = 190;
            this.direccionRText.Visible = false;
            // 
            // direccionRTitle
            // 
            this.direccionRTitle.AutoSize = true;
            this.direccionRTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.direccionRTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.direccionRTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.direccionRTitle.Location = new System.Drawing.Point(26, 597);
            this.direccionRTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.direccionRTitle.Name = "direccionRTitle";
            this.direccionRTitle.Size = new System.Drawing.Size(101, 22);
            this.direccionRTitle.TabIndex = 189;
            this.direccionRTitle.Text = "Dirección:";
            this.direccionRTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.direccionRTitle.Visible = false;
            // 
            // Telefono1Text
            // 
            this.Telefono1Text.Enabled = false;
            this.Telefono1Text.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Telefono1Text.Location = new System.Drawing.Point(168, 648);
            this.Telefono1Text.Name = "Telefono1Text";
            this.Telefono1Text.Size = new System.Drawing.Size(244, 31);
            this.Telefono1Text.TabIndex = 188;
            this.Telefono1Text.Visible = false;
            // 
            // telefono1Title
            // 
            this.telefono1Title.AutoSize = true;
            this.telefono1Title.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.telefono1Title.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.telefono1Title.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.telefono1Title.Location = new System.Drawing.Point(26, 651);
            this.telefono1Title.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.telefono1Title.Name = "telefono1Title";
            this.telefono1Title.Size = new System.Drawing.Size(125, 22);
            this.telefono1Title.TabIndex = 187;
            this.telefono1Title.Text = "1er teléfono:";
            this.telefono1Title.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.telefono1Title.Visible = false;
            // 
            // claveRText
            // 
            this.claveRText.Enabled = false;
            this.claveRText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.claveRText.Location = new System.Drawing.Point(111, 539);
            this.claveRText.Name = "claveRText";
            this.claveRText.Size = new System.Drawing.Size(108, 31);
            this.claveRText.TabIndex = 186;
            this.claveRText.Visible = false;
            // 
            // claveRTitle
            // 
            this.claveRTitle.AutoSize = true;
            this.claveRTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.claveRTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.claveRTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.claveRTitle.Location = new System.Drawing.Point(26, 542);
            this.claveRTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.claveRTitle.Name = "claveRTitle";
            this.claveRTitle.Size = new System.Drawing.Size(71, 22);
            this.claveRTitle.TabIndex = 185;
            this.claveRTitle.Text = "Clave:";
            this.claveRTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.claveRTitle.Visible = false;
            // 
            // carreraTitle
            // 
            this.carreraTitle.AutoSize = true;
            this.carreraTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.carreraTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.carreraTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.carreraTitle.Location = new System.Drawing.Point(443, 299);
            this.carreraTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.carreraTitle.Name = "carreraTitle";
            this.carreraTitle.Size = new System.Drawing.Size(136, 22);
            this.carreraTitle.TabIndex = 184;
            this.carreraTitle.Text = "Siglas carrera:";
            this.carreraTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.carreraTitle.Visible = false;
            // 
            // seguroText
            // 
            this.seguroText.Enabled = false;
            this.seguroText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.seguroText.Location = new System.Drawing.Point(944, 296);
            this.seguroText.Name = "seguroText";
            this.seguroText.Size = new System.Drawing.Size(198, 31);
            this.seguroText.TabIndex = 183;
            this.seguroText.Visible = false;
            // 
            // seguroTitle
            // 
            this.seguroTitle.AutoSize = true;
            this.seguroTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.seguroTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.seguroTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.seguroTitle.Location = new System.Drawing.Point(782, 299);
            this.seguroTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.seguroTitle.Name = "seguroTitle";
            this.seguroTitle.Size = new System.Drawing.Size(150, 22);
            this.seguroTitle.TabIndex = 182;
            this.seguroTitle.Text = "Cédula seguro:";
            this.seguroTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.seguroTitle.Visible = false;
            // 
            // datosTitle
            // 
            this.datosTitle.AutoSize = true;
            this.datosTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.datosTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.datosTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.datosTitle.Location = new System.Drawing.Point(26, 495);
            this.datosTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.datosTitle.Name = "datosTitle";
            this.datosTitle.Size = new System.Drawing.Size(279, 23);
            this.datosTitle.TabIndex = 181;
            this.datosTitle.Text = "Datos resposable del alumno";
            this.datosTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.datosTitle.Visible = false;
            // 
            // nacionalidadText
            // 
            this.nacionalidadText.Enabled = false;
            this.nacionalidadText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nacionalidadText.Location = new System.Drawing.Point(783, 445);
            this.nacionalidadText.Name = "nacionalidadText";
            this.nacionalidadText.Size = new System.Drawing.Size(223, 31);
            this.nacionalidadText.TabIndex = 165;
            this.nacionalidadText.Visible = false;
            // 
            // nacionalidadTitle
            // 
            this.nacionalidadTitle.AutoSize = true;
            this.nacionalidadTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.nacionalidadTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nacionalidadTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.nacionalidadTitle.Location = new System.Drawing.Point(632, 448);
            this.nacionalidadTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.nacionalidadTitle.Name = "nacionalidadTitle";
            this.nacionalidadTitle.Size = new System.Drawing.Size(139, 22);
            this.nacionalidadTitle.TabIndex = 164;
            this.nacionalidadTitle.Text = "Nacionalidad:";
            this.nacionalidadTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.nacionalidadTitle.Visible = false;
            // 
            // anoText
            // 
            this.anoText.Enabled = false;
            this.anoText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.anoText.Location = new System.Drawing.Point(387, 387);
            this.anoText.Name = "anoText";
            this.anoText.Size = new System.Drawing.Size(223, 31);
            this.anoText.TabIndex = 162;
            this.anoText.Visible = false;
            // 
            // anoTitle
            // 
            this.anoTitle.AutoSize = true;
            this.anoTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.anoTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.anoTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.anoTitle.Location = new System.Drawing.Point(319, 390);
            this.anoTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.anoTitle.Name = "anoTitle";
            this.anoTitle.Size = new System.Drawing.Size(54, 22);
            this.anoTitle.TabIndex = 161;
            this.anoTitle.Text = "Año:";
            this.anoTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.anoTitle.Visible = false;
            // 
            // estadoNText
            // 
            this.estadoNText.Enabled = false;
            this.estadoNText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.estadoNText.Location = new System.Drawing.Point(783, 387);
            this.estadoNText.Name = "estadoNText";
            this.estadoNText.Size = new System.Drawing.Size(223, 31);
            this.estadoNText.TabIndex = 160;
            this.estadoNText.Visible = false;
            // 
            // estadoNTitle
            // 
            this.estadoNTitle.AutoSize = true;
            this.estadoNTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.estadoNTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.estadoNTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.estadoNTitle.Location = new System.Drawing.Point(632, 390);
            this.estadoNTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.estadoNTitle.Name = "estadoNTitle";
            this.estadoNTitle.Size = new System.Drawing.Size(77, 22);
            this.estadoNTitle.TabIndex = 159;
            this.estadoNTitle.Text = "Estado:";
            this.estadoNTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.estadoNTitle.Visible = false;
            // 
            // paisNText
            // 
            this.paisNText.Enabled = false;
            this.paisNText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.paisNText.Location = new System.Drawing.Point(387, 439);
            this.paisNText.Name = "paisNText";
            this.paisNText.Size = new System.Drawing.Size(223, 31);
            this.paisNText.TabIndex = 158;
            this.paisNText.Visible = false;
            // 
            // paisNTitle
            // 
            this.paisNTitle.AutoSize = true;
            this.paisNTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.paisNTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.paisNTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.paisNTitle.Location = new System.Drawing.Point(319, 442);
            this.paisNTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.paisNTitle.Name = "paisNTitle";
            this.paisNTitle.Size = new System.Drawing.Size(49, 22);
            this.paisNTitle.TabIndex = 157;
            this.paisNTitle.Text = "País:";
            this.paisNTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.paisNTitle.Visible = false;
            // 
            // mesText
            // 
            this.mesText.Enabled = false;
            this.mesText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mesText.Location = new System.Drawing.Point(86, 439);
            this.mesText.Name = "mesText";
            this.mesText.Size = new System.Drawing.Size(208, 31);
            this.mesText.TabIndex = 156;
            this.mesText.Visible = false;
            // 
            // mesTitle
            // 
            this.mesTitle.AutoSize = true;
            this.mesTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.mesTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mesTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.mesTitle.Location = new System.Drawing.Point(26, 445);
            this.mesTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.mesTitle.Name = "mesTitle";
            this.mesTitle.Size = new System.Drawing.Size(51, 22);
            this.mesTitle.TabIndex = 155;
            this.mesTitle.Text = "Mes:";
            this.mesTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.mesTitle.Visible = false;
            // 
            // diaText
            // 
            this.diaText.Enabled = false;
            this.diaText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.diaText.Location = new System.Drawing.Point(86, 387);
            this.diaText.Name = "diaText";
            this.diaText.Size = new System.Drawing.Size(208, 31);
            this.diaText.TabIndex = 154;
            this.diaText.Visible = false;
            // 
            // diaTitle
            // 
            this.diaTitle.AutoSize = true;
            this.diaTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.diaTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.diaTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.diaTitle.Location = new System.Drawing.Point(26, 390);
            this.diaTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.diaTitle.Name = "diaTitle";
            this.diaTitle.Size = new System.Drawing.Size(45, 22);
            this.diaTitle.TabIndex = 153;
            this.diaTitle.Text = "Día:";
            this.diaTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.diaTitle.Visible = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label14.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label14.Location = new System.Drawing.Point(26, 352);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(276, 23);
            this.label14.TabIndex = 152;
            this.label14.Text = "Fecha y lugar de nacimiento";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label14.Visible = false;
            // 
            // apellidoMText
            // 
            this.apellidoMText.Enabled = false;
            this.apellidoMText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.apellidoMText.Location = new System.Drawing.Point(207, 296);
            this.apellidoMText.Name = "apellidoMText";
            this.apellidoMText.Size = new System.Drawing.Size(211, 31);
            this.apellidoMText.TabIndex = 151;
            this.apellidoMText.Visible = false;
            // 
            // apellidoMTitle
            // 
            this.apellidoMTitle.AutoSize = true;
            this.apellidoMTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.apellidoMTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.apellidoMTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.apellidoMTitle.Location = new System.Drawing.Point(26, 299);
            this.apellidoMTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.apellidoMTitle.Name = "apellidoMTitle";
            this.apellidoMTitle.Size = new System.Drawing.Size(173, 22);
            this.apellidoMTitle.TabIndex = 150;
            this.apellidoMTitle.Text = "Apellido Materno:";
            this.apellidoMTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.apellidoMTitle.Visible = false;
            // 
            // apellidoPText
            // 
            this.apellidoPText.Enabled = false;
            this.apellidoPText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.apellidoPText.Location = new System.Drawing.Point(541, 241);
            this.apellidoPText.Name = "apellidoPText";
            this.apellidoPText.Size = new System.Drawing.Size(211, 31);
            this.apellidoPText.TabIndex = 149;
            this.apellidoPText.Visible = false;
            // 
            // apellidoPTitle
            // 
            this.apellidoPTitle.AutoSize = true;
            this.apellidoPTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.apellidoPTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.apellidoPTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.apellidoPTitle.Location = new System.Drawing.Point(365, 244);
            this.apellidoPTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.apellidoPTitle.Name = "apellidoPTitle";
            this.apellidoPTitle.Size = new System.Drawing.Size(167, 22);
            this.apellidoPTitle.TabIndex = 148;
            this.apellidoPTitle.Text = "Apellido Paterno:";
            this.apellidoPTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.apellidoPTitle.Visible = false;
            // 
            // nombreText
            // 
            this.nombreText.Enabled = false;
            this.nombreText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nombreText.Location = new System.Drawing.Point(131, 241);
            this.nombreText.Name = "nombreText";
            this.nombreText.Size = new System.Drawing.Size(219, 31);
            this.nombreText.TabIndex = 147;
            this.nombreText.Visible = false;
            // 
            // nombreTitle
            // 
            this.nombreTitle.AutoSize = true;
            this.nombreTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.nombreTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nombreTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.nombreTitle.Location = new System.Drawing.Point(26, 244);
            this.nombreTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.nombreTitle.Name = "nombreTitle";
            this.nombreTitle.Size = new System.Drawing.Size(89, 22);
            this.nombreTitle.TabIndex = 146;
            this.nombreTitle.Text = "Nombre:";
            this.nombreTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.nombreTitle.Visible = false;
            // 
            // telefonosText
            // 
            this.telefonosText.Enabled = false;
            this.telefonosText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.telefonosText.Location = new System.Drawing.Point(873, 241);
            this.telefonosText.Name = "telefonosText";
            this.telefonosText.Size = new System.Drawing.Size(269, 31);
            this.telefonosText.TabIndex = 203;
            this.telefonosText.Visible = false;
            this.telefonosText.TextChanged += new System.EventHandler(this.telefonosText_TextChanged);
            // 
            // carreraText
            // 
            this.carreraText.Enabled = false;
            this.carreraText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.carreraText.Location = new System.Drawing.Point(591, 296);
            this.carreraText.Name = "carreraText";
            this.carreraText.Size = new System.Drawing.Size(166, 31);
            this.carreraText.TabIndex = 204;
            this.carreraText.Visible = false;
            // 
            // ConsultaAlumno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1159, 708);
            this.Controls.Add(this.carreraText);
            this.Controls.Add(this.telefonosText);
            this.Controls.Add(this.telefonosTitle);
            this.Controls.Add(this.mail1Text);
            this.Controls.Add(this.mail1Title);
            this.Controls.Add(this.mail2Text);
            this.Controls.Add(this.mail2Title);
            this.Controls.Add(this.telefono2Text);
            this.Controls.Add(this.telefono2Title);
            this.Controls.Add(this.nombreRText);
            this.Controls.Add(this.nombreRTitle);
            this.Controls.Add(this.direccionRText);
            this.Controls.Add(this.direccionRTitle);
            this.Controls.Add(this.Telefono1Text);
            this.Controls.Add(this.telefono1Title);
            this.Controls.Add(this.claveRText);
            this.Controls.Add(this.claveRTitle);
            this.Controls.Add(this.carreraTitle);
            this.Controls.Add(this.seguroText);
            this.Controls.Add(this.seguroTitle);
            this.Controls.Add(this.datosTitle);
            this.Controls.Add(this.nacionalidadText);
            this.Controls.Add(this.nacionalidadTitle);
            this.Controls.Add(this.anoText);
            this.Controls.Add(this.anoTitle);
            this.Controls.Add(this.estadoNText);
            this.Controls.Add(this.estadoNTitle);
            this.Controls.Add(this.paisNText);
            this.Controls.Add(this.paisNTitle);
            this.Controls.Add(this.mesText);
            this.Controls.Add(this.mesTitle);
            this.Controls.Add(this.diaText);
            this.Controls.Add(this.diaTitle);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.apellidoMText);
            this.Controls.Add(this.apellidoMTitle);
            this.Controls.Add(this.apellidoPText);
            this.Controls.Add(this.apellidoPTitle);
            this.Controls.Add(this.nombreText);
            this.Controls.Add(this.nombreTitle);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.matriculaText);
            this.Controls.Add(this.nominaTitle);
            this.Controls.Add(this.regresarBoton);
            this.Controls.Add(this.ejecutarBoton);
            this.Controls.Add(this.LaberQuerie);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox2);
            this.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.Name = "ConsultaAlumno";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ConsultaAlumno";
            this.Load += new System.EventHandler(this.ConsultaAlumno_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LaberQuerie;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button regresarBoton;
        private System.Windows.Forms.Button ejecutarBoton;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox matriculaText;
        private System.Windows.Forms.Label nominaTitle;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label telefonosTitle;
        private System.Windows.Forms.TextBox mail1Text;
        private System.Windows.Forms.Label mail1Title;
        private System.Windows.Forms.TextBox mail2Text;
        private System.Windows.Forms.Label mail2Title;
        private System.Windows.Forms.TextBox telefono2Text;
        private System.Windows.Forms.Label telefono2Title;
        private System.Windows.Forms.TextBox nombreRText;
        private System.Windows.Forms.Label nombreRTitle;
        private System.Windows.Forms.TextBox direccionRText;
        private System.Windows.Forms.Label direccionRTitle;
        private System.Windows.Forms.TextBox Telefono1Text;
        private System.Windows.Forms.Label telefono1Title;
        private System.Windows.Forms.TextBox claveRText;
        private System.Windows.Forms.Label claveRTitle;
        private System.Windows.Forms.Label carreraTitle;
        private System.Windows.Forms.TextBox seguroText;
        private System.Windows.Forms.Label seguroTitle;
        private System.Windows.Forms.Label datosTitle;
        private System.Windows.Forms.TextBox nacionalidadText;
        private System.Windows.Forms.Label nacionalidadTitle;
        private System.Windows.Forms.TextBox anoText;
        private System.Windows.Forms.Label anoTitle;
        private System.Windows.Forms.TextBox estadoNText;
        private System.Windows.Forms.Label estadoNTitle;
        private System.Windows.Forms.TextBox paisNText;
        private System.Windows.Forms.Label paisNTitle;
        private System.Windows.Forms.TextBox mesText;
        private System.Windows.Forms.Label mesTitle;
        private System.Windows.Forms.TextBox diaText;
        private System.Windows.Forms.Label diaTitle;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox apellidoMText;
        private System.Windows.Forms.Label apellidoMTitle;
        private System.Windows.Forms.TextBox apellidoPText;
        private System.Windows.Forms.Label apellidoPTitle;
        private System.Windows.Forms.TextBox nombreText;
        private System.Windows.Forms.Label nombreTitle;
        private System.Windows.Forms.TextBox telefonosText;
        private System.Windows.Forms.TextBox carreraText;
    }
}