﻿namespace QueriesDisplayer
{
    partial class AltaMateria
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AltaMateria));
            this.label6 = new System.Windows.Forms.Label();
            this.LaberQuerie = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.claveText = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.abrevText = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.nombreText = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.unidadesText = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.carreraText = new System.Windows.Forms.ComboBox();
            this.departamentoText = new System.Windows.Forms.ComboBox();
            this.cipText = new System.Windows.Forms.ComboBox();
            this.regresarBoton = new System.Windows.Forms.Button();
            this.ejecutarBoton = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.temasText = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label6.Location = new System.Drawing.Point(33, 122);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(189, 23);
            this.label6.TabIndex = 64;
            this.label6.Text = "Información básica";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // LaberQuerie
            // 
            this.LaberQuerie.AutoSize = true;
            this.LaberQuerie.BackColor = System.Drawing.Color.Transparent;
            this.LaberQuerie.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LaberQuerie.Font = new System.Drawing.Font("Century Gothic", 24F);
            this.LaberQuerie.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.LaberQuerie.Location = new System.Drawing.Point(514, -165);
            this.LaberQuerie.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LaberQuerie.Name = "LaberQuerie";
            this.LaberQuerie.Size = new System.Drawing.Size(524, 39);
            this.LaberQuerie.TabIndex = 62;
            this.LaberQuerie.Text = "Base de datos TEC - Alta materia";
            this.LaberQuerie.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.LaberQuerie.Click += new System.EventHandler(this.LaberQuerie_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(109)))), ((int)(((byte)(188)))));
            this.pictureBox1.ErrorImage = null;
            this.pictureBox1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1159, 100);
            this.pictureBox1.TabIndex = 63;
            this.pictureBox1.TabStop = false;
            // 
            // claveText
            // 
            this.claveText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.claveText.Location = new System.Drawing.Point(124, 165);
            this.claveText.Name = "claveText";
            this.claveText.Size = new System.Drawing.Size(198, 31);
            this.claveText.TabIndex = 99;
            this.claveText.TextChanged += new System.EventHandler(this.claveText_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label7.Location = new System.Drawing.Point(33, 168);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 22);
            this.label7.TabIndex = 98;
            this.label7.Text = "Clave:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // abrevText
            // 
            this.abrevText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.abrevText.Location = new System.Drawing.Point(166, 224);
            this.abrevText.Name = "abrevText";
            this.abrevText.Size = new System.Drawing.Size(156, 31);
            this.abrevText.TabIndex = 101;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label1.Location = new System.Drawing.Point(33, 227);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 22);
            this.label1.TabIndex = 100;
            this.label1.Text = "Abreviatura:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label2.Location = new System.Drawing.Point(333, 168);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 22);
            this.label2.TabIndex = 102;
            this.label2.Text = "Nombre:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // nombreText
            // 
            this.nombreText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nombreText.Location = new System.Drawing.Point(437, 165);
            this.nombreText.Name = "nombreText";
            this.nombreText.Size = new System.Drawing.Size(481, 31);
            this.nombreText.TabIndex = 103;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label3.Location = new System.Drawing.Point(33, 368);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 22);
            this.label3.TabIndex = 104;
            this.label3.Text = "CIP:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label4.Location = new System.Drawing.Point(367, 227);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 22);
            this.label4.TabIndex = 106;
            this.label4.Text = "Carrera:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // unidadesText
            // 
            this.unidadesText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.unidadesText.Location = new System.Drawing.Point(780, 224);
            this.unidadesText.Name = "unidadesText";
            this.unidadesText.Size = new System.Drawing.Size(156, 31);
            this.unidadesText.TabIndex = 109;
            this.unidadesText.TextChanged += new System.EventHandler(this.unidadesText_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label5.Location = new System.Drawing.Point(665, 227);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 22);
            this.label5.TabIndex = 108;
            this.label5.Text = "Unidades:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label8.Location = new System.Drawing.Point(333, 368);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(153, 22);
            this.label8.TabIndex = 110;
            this.label8.Text = "Departamento:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 24F);
            this.label9.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label9.Location = new System.Drawing.Point(229, 27);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(524, 39);
            this.label9.TabIndex = 112;
            this.label9.Text = "Base de datos TEC - Alta materia";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // carreraText
            // 
            this.carreraText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.carreraText.FormattingEnabled = true;
            this.carreraText.Location = new System.Drawing.Point(467, 224);
            this.carreraText.Name = "carreraText";
            this.carreraText.Size = new System.Drawing.Size(170, 30);
            this.carreraText.TabIndex = 113;
            // 
            // departamentoText
            // 
            this.departamentoText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.departamentoText.FormattingEnabled = true;
            this.departamentoText.Location = new System.Drawing.Point(504, 365);
            this.departamentoText.Name = "departamentoText";
            this.departamentoText.Size = new System.Drawing.Size(233, 30);
            this.departamentoText.TabIndex = 114;
            // 
            // cipText
            // 
            this.cipText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cipText.FormattingEnabled = true;
            this.cipText.Location = new System.Drawing.Point(95, 365);
            this.cipText.Name = "cipText";
            this.cipText.Size = new System.Drawing.Size(212, 30);
            this.cipText.TabIndex = 115;
            this.cipText.SelectedIndexChanged += new System.EventHandler(this.cipText_SelectedIndexChanged);
            // 
            // regresarBoton
            // 
            this.regresarBoton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(109)))), ((int)(((byte)(188)))));
            this.regresarBoton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.regresarBoton.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.regresarBoton.ForeColor = System.Drawing.Color.White;
            this.regresarBoton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.regresarBoton.Location = new System.Drawing.Point(780, 363);
            this.regresarBoton.Name = "regresarBoton";
            this.regresarBoton.Size = new System.Drawing.Size(148, 41);
            this.regresarBoton.TabIndex = 117;
            this.regresarBoton.Text = "Regresar";
            this.regresarBoton.UseVisualStyleBackColor = false;
            this.regresarBoton.Click += new System.EventHandler(this.regresarBoton_Click);
            // 
            // ejecutarBoton
            // 
            this.ejecutarBoton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(109)))), ((int)(((byte)(188)))));
            this.ejecutarBoton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ejecutarBoton.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ejecutarBoton.ForeColor = System.Drawing.Color.White;
            this.ejecutarBoton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ejecutarBoton.Location = new System.Drawing.Point(780, 296);
            this.ejecutarBoton.Name = "ejecutarBoton";
            this.ejecutarBoton.Size = new System.Drawing.Size(148, 41);
            this.ejecutarBoton.TabIndex = 116;
            this.ejecutarBoton.Text = "Ejecutar";
            this.ejecutarBoton.UseVisualStyleBackColor = false;
            this.ejecutarBoton.Click += new System.EventHandler(this.ejecutarBoton_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label10.Location = new System.Drawing.Point(33, 315);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(72, 22);
            this.label10.TabIndex = 119;
            this.label10.Text = "Temas:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label10.Click += new System.EventHandler(this.label10_Click);
            // 
            // temasText
            // 
            this.temasText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.temasText.Location = new System.Drawing.Point(124, 311);
            this.temasText.Name = "temasText";
            this.temasText.Size = new System.Drawing.Size(613, 31);
            this.temasText.TabIndex = 120;
            this.temasText.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label11.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label11.Location = new System.Drawing.Point(33, 277);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(240, 19);
            this.label11.TabIndex = 121;
            this.label11.Text = "* Separar múltiples temas con \",\" *";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // AltaMateria
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(963, 437);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.temasText);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.regresarBoton);
            this.Controls.Add(this.ejecutarBoton);
            this.Controls.Add(this.cipText);
            this.Controls.Add(this.departamentoText);
            this.Controls.Add(this.carreraText);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.unidadesText);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.nombreText);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.abrevText);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.claveText);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.LaberQuerie);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AltaMateria";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AltaMateria";
            this.Load += new System.EventHandler(this.AltaMateria_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label LaberQuerie;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox claveText;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox abrevText;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox nombreText;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox unidadesText;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox carreraText;
        private System.Windows.Forms.ComboBox departamentoText;
        private System.Windows.Forms.ComboBox cipText;
        private System.Windows.Forms.Button regresarBoton;
        private System.Windows.Forms.Button ejecutarBoton;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox temasText;
        private System.Windows.Forms.Label label11;
    }
}