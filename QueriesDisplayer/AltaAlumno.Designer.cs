﻿namespace QueriesDisplayer
{
    partial class AltaAlumno
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AltaAlumno));
            this.LaberQuerie = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.anoText = new System.Windows.Forms.TextBox();
            this.anoTitle = new System.Windows.Forms.Label();
            this.estadoNText = new System.Windows.Forms.TextBox();
            this.estadoNTitle = new System.Windows.Forms.Label();
            this.paisNText = new System.Windows.Forms.TextBox();
            this.paisNTitle = new System.Windows.Forms.Label();
            this.mesText = new System.Windows.Forms.TextBox();
            this.mesTitle = new System.Windows.Forms.Label();
            this.diaText = new System.Windows.Forms.TextBox();
            this.diaTitle = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.apellidoMText = new System.Windows.Forms.TextBox();
            this.apellidoMTitle = new System.Windows.Forms.Label();
            this.apellidoPText = new System.Windows.Forms.TextBox();
            this.apellidoPTitle = new System.Windows.Forms.Label();
            this.nombreText = new System.Windows.Forms.TextBox();
            this.nombreTitle = new System.Windows.Forms.Label();
            this.matriculaText = new System.Windows.Forms.TextBox();
            this.nominaTitle = new System.Windows.Forms.Label();
            this.nacionalidadText = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.ciudadText = new System.Windows.Forms.TextBox();
            this.ciudadTitle = new System.Windows.Forms.Label();
            this.coloniaText = new System.Windows.Forms.TextBox();
            this.coloniaTitle = new System.Windows.Forms.Label();
            this.cpText = new System.Windows.Forms.TextBox();
            this.cpTitle = new System.Windows.Forms.Label();
            this.estadoDText = new System.Windows.Forms.TextBox();
            this.estadoDTitle = new System.Windows.Forms.Label();
            this.paisDText = new System.Windows.Forms.TextBox();
            this.paisDTitle = new System.Windows.Forms.Label();
            this.numeroText = new System.Windows.Forms.TextBox();
            this.numeroTitle = new System.Windows.Forms.Label();
            this.calleText = new System.Windows.Forms.TextBox();
            this.calleTitle = new System.Windows.Forms.Label();
            this.direccionTitle = new System.Windows.Forms.Label();
            this.datosTitle = new System.Windows.Forms.Label();
            this.seguroText = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.direccionRText = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.Telefono1Text = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.claveRText = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.nombreRText = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.telefono2Text = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.mail2Text = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.mail1Text = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.carreraText = new System.Windows.Forms.ComboBox();
            this.regresarBoton = new System.Windows.Forms.Button();
            this.ejecutarBoton = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.telefonosText = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // LaberQuerie
            // 
            this.LaberQuerie.AutoSize = true;
            this.LaberQuerie.BackColor = System.Drawing.Color.Transparent;
            this.LaberQuerie.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LaberQuerie.Font = new System.Drawing.Font("Century Gothic", 24F);
            this.LaberQuerie.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.LaberQuerie.Location = new System.Drawing.Point(323, 27);
            this.LaberQuerie.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LaberQuerie.Name = "LaberQuerie";
            this.LaberQuerie.Size = new System.Drawing.Size(520, 39);
            this.LaberQuerie.TabIndex = 20;
            this.LaberQuerie.Text = "Base de datos TEC - Alta alumno";
            this.LaberQuerie.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(109)))), ((int)(((byte)(188)))));
            this.pictureBox1.ErrorImage = null;
            this.pictureBox1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1159, 100);
            this.pictureBox1.TabIndex = 22;
            this.pictureBox1.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label6.Location = new System.Drawing.Point(17, 110);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(189, 23);
            this.label6.TabIndex = 61;
            this.label6.Text = "Información básica";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // anoText
            // 
            this.anoText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.anoText.Location = new System.Drawing.Point(378, 315);
            this.anoText.Name = "anoText";
            this.anoText.Size = new System.Drawing.Size(223, 31);
            this.anoText.TabIndex = 60;
            // 
            // anoTitle
            // 
            this.anoTitle.AutoSize = true;
            this.anoTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.anoTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.anoTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.anoTitle.Location = new System.Drawing.Point(310, 318);
            this.anoTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.anoTitle.Name = "anoTitle";
            this.anoTitle.Size = new System.Drawing.Size(54, 22);
            this.anoTitle.TabIndex = 59;
            this.anoTitle.Text = "Año:";
            this.anoTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // estadoNText
            // 
            this.estadoNText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.estadoNText.Location = new System.Drawing.Point(774, 315);
            this.estadoNText.Name = "estadoNText";
            this.estadoNText.Size = new System.Drawing.Size(223, 31);
            this.estadoNText.TabIndex = 58;
            // 
            // estadoNTitle
            // 
            this.estadoNTitle.AutoSize = true;
            this.estadoNTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.estadoNTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.estadoNTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.estadoNTitle.Location = new System.Drawing.Point(623, 318);
            this.estadoNTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.estadoNTitle.Name = "estadoNTitle";
            this.estadoNTitle.Size = new System.Drawing.Size(77, 22);
            this.estadoNTitle.TabIndex = 57;
            this.estadoNTitle.Text = "Estado:";
            this.estadoNTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // paisNText
            // 
            this.paisNText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.paisNText.Location = new System.Drawing.Point(378, 367);
            this.paisNText.Name = "paisNText";
            this.paisNText.Size = new System.Drawing.Size(223, 31);
            this.paisNText.TabIndex = 56;
            // 
            // paisNTitle
            // 
            this.paisNTitle.AutoSize = true;
            this.paisNTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.paisNTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.paisNTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.paisNTitle.Location = new System.Drawing.Point(310, 370);
            this.paisNTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.paisNTitle.Name = "paisNTitle";
            this.paisNTitle.Size = new System.Drawing.Size(49, 22);
            this.paisNTitle.TabIndex = 55;
            this.paisNTitle.Text = "País:";
            this.paisNTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // mesText
            // 
            this.mesText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mesText.Location = new System.Drawing.Point(77, 367);
            this.mesText.Name = "mesText";
            this.mesText.Size = new System.Drawing.Size(208, 31);
            this.mesText.TabIndex = 54;
            // 
            // mesTitle
            // 
            this.mesTitle.AutoSize = true;
            this.mesTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.mesTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mesTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.mesTitle.Location = new System.Drawing.Point(17, 373);
            this.mesTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.mesTitle.Name = "mesTitle";
            this.mesTitle.Size = new System.Drawing.Size(51, 22);
            this.mesTitle.TabIndex = 53;
            this.mesTitle.Text = "Mes:";
            this.mesTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // diaText
            // 
            this.diaText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.diaText.Location = new System.Drawing.Point(77, 315);
            this.diaText.Name = "diaText";
            this.diaText.Size = new System.Drawing.Size(208, 31);
            this.diaText.TabIndex = 52;
            // 
            // diaTitle
            // 
            this.diaTitle.AutoSize = true;
            this.diaTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.diaTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.diaTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.diaTitle.Location = new System.Drawing.Point(17, 318);
            this.diaTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.diaTitle.Name = "diaTitle";
            this.diaTitle.Size = new System.Drawing.Size(45, 22);
            this.diaTitle.TabIndex = 51;
            this.diaTitle.Text = "Día:";
            this.diaTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label1.Location = new System.Drawing.Point(17, 280);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(276, 23);
            this.label1.TabIndex = 50;
            this.label1.Text = "Fecha y lugar de nacimiento";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // apellidoMText
            // 
            this.apellidoMText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.apellidoMText.Location = new System.Drawing.Point(537, 206);
            this.apellidoMText.Name = "apellidoMText";
            this.apellidoMText.Size = new System.Drawing.Size(211, 31);
            this.apellidoMText.TabIndex = 49;
            // 
            // apellidoMTitle
            // 
            this.apellidoMTitle.AutoSize = true;
            this.apellidoMTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.apellidoMTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.apellidoMTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.apellidoMTitle.Location = new System.Drawing.Point(356, 209);
            this.apellidoMTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.apellidoMTitle.Name = "apellidoMTitle";
            this.apellidoMTitle.Size = new System.Drawing.Size(173, 22);
            this.apellidoMTitle.TabIndex = 48;
            this.apellidoMTitle.Text = "Apellido Materno:";
            this.apellidoMTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // apellidoPText
            // 
            this.apellidoPText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.apellidoPText.Location = new System.Drawing.Point(537, 151);
            this.apellidoPText.Name = "apellidoPText";
            this.apellidoPText.Size = new System.Drawing.Size(211, 31);
            this.apellidoPText.TabIndex = 47;
            // 
            // apellidoPTitle
            // 
            this.apellidoPTitle.AutoSize = true;
            this.apellidoPTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.apellidoPTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.apellidoPTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.apellidoPTitle.Location = new System.Drawing.Point(356, 154);
            this.apellidoPTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.apellidoPTitle.Name = "apellidoPTitle";
            this.apellidoPTitle.Size = new System.Drawing.Size(167, 22);
            this.apellidoPTitle.TabIndex = 46;
            this.apellidoPTitle.Text = "Apellido Paterno:";
            this.apellidoPTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // nombreText
            // 
            this.nombreText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nombreText.Location = new System.Drawing.Point(122, 206);
            this.nombreText.Name = "nombreText";
            this.nombreText.Size = new System.Drawing.Size(219, 31);
            this.nombreText.TabIndex = 45;
            // 
            // nombreTitle
            // 
            this.nombreTitle.AutoSize = true;
            this.nombreTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.nombreTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nombreTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.nombreTitle.Location = new System.Drawing.Point(17, 209);
            this.nombreTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.nombreTitle.Name = "nombreTitle";
            this.nombreTitle.Size = new System.Drawing.Size(89, 22);
            this.nombreTitle.TabIndex = 44;
            this.nombreTitle.Text = "Nombre:";
            this.nombreTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // matriculaText
            // 
            this.matriculaText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matriculaText.Location = new System.Drawing.Point(122, 151);
            this.matriculaText.Name = "matriculaText";
            this.matriculaText.Size = new System.Drawing.Size(219, 31);
            this.matriculaText.TabIndex = 43;
            this.matriculaText.TextChanged += new System.EventHandler(this.matriculaText_TextChanged);
            // 
            // nominaTitle
            // 
            this.nominaTitle.AutoSize = true;
            this.nominaTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.nominaTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nominaTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.nominaTitle.Location = new System.Drawing.Point(17, 154);
            this.nominaTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.nominaTitle.Name = "nominaTitle";
            this.nominaTitle.Size = new System.Drawing.Size(101, 22);
            this.nominaTitle.TabIndex = 42;
            this.nominaTitle.Text = "Matrícula:";
            this.nominaTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // nacionalidadText
            // 
            this.nacionalidadText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nacionalidadText.Location = new System.Drawing.Point(774, 373);
            this.nacionalidadText.Name = "nacionalidadText";
            this.nacionalidadText.Size = new System.Drawing.Size(223, 31);
            this.nacionalidadText.TabIndex = 75;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label3.Location = new System.Drawing.Point(623, 376);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(139, 22);
            this.label3.TabIndex = 74;
            this.label3.Text = "Nacionalidad:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ciudadText
            // 
            this.ciudadText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ciudadText.Location = new System.Drawing.Point(505, 464);
            this.ciudadText.Name = "ciudadText";
            this.ciudadText.Size = new System.Drawing.Size(243, 31);
            this.ciudadText.TabIndex = 90;
            // 
            // ciudadTitle
            // 
            this.ciudadTitle.AutoSize = true;
            this.ciudadTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ciudadTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ciudadTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ciudadTitle.Location = new System.Drawing.Point(392, 467);
            this.ciudadTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.ciudadTitle.Name = "ciudadTitle";
            this.ciudadTitle.Size = new System.Drawing.Size(84, 22);
            this.ciudadTitle.TabIndex = 89;
            this.ciudadTitle.Text = "Ciudad:";
            this.ciudadTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // coloniaText
            // 
            this.coloniaText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.coloniaText.Location = new System.Drawing.Point(329, 513);
            this.coloniaText.Name = "coloniaText";
            this.coloniaText.Size = new System.Drawing.Size(244, 31);
            this.coloniaText.TabIndex = 88;
            // 
            // coloniaTitle
            // 
            this.coloniaTitle.AutoSize = true;
            this.coloniaTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.coloniaTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.coloniaTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.coloniaTitle.Location = new System.Drawing.Point(239, 519);
            this.coloniaTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.coloniaTitle.Name = "coloniaTitle";
            this.coloniaTitle.Size = new System.Drawing.Size(85, 22);
            this.coloniaTitle.TabIndex = 87;
            this.coloniaTitle.Text = "Colonia:";
            this.coloniaTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cpText
            // 
            this.cpText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cpText.Location = new System.Drawing.Point(1008, 513);
            this.cpText.Name = "cpText";
            this.cpText.Size = new System.Drawing.Size(107, 31);
            this.cpText.TabIndex = 86;
            // 
            // cpTitle
            // 
            this.cpTitle.AutoSize = true;
            this.cpTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cpTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cpTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.cpTitle.Location = new System.Drawing.Point(952, 519);
            this.cpTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.cpTitle.Name = "cpTitle";
            this.cpTitle.Size = new System.Drawing.Size(41, 22);
            this.cpTitle.TabIndex = 85;
            this.cpTitle.Text = "CP:";
            this.cpTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // estadoDText
            // 
            this.estadoDText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.estadoDText.Location = new System.Drawing.Point(677, 513);
            this.estadoDText.Name = "estadoDText";
            this.estadoDText.Size = new System.Drawing.Size(244, 31);
            this.estadoDText.TabIndex = 84;
            // 
            // estadoDTitle
            // 
            this.estadoDTitle.AutoSize = true;
            this.estadoDTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.estadoDTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.estadoDTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.estadoDTitle.Location = new System.Drawing.Point(591, 519);
            this.estadoDTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.estadoDTitle.Name = "estadoDTitle";
            this.estadoDTitle.Size = new System.Drawing.Size(77, 22);
            this.estadoDTitle.TabIndex = 83;
            this.estadoDTitle.Text = "Estado:";
            this.estadoDTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // paisDText
            // 
            this.paisDText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.paisDText.Location = new System.Drawing.Point(837, 461);
            this.paisDText.Name = "paisDText";
            this.paisDText.Size = new System.Drawing.Size(230, 31);
            this.paisDText.TabIndex = 82;
            // 
            // paisDTitle
            // 
            this.paisDTitle.AutoSize = true;
            this.paisDTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.paisDTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.paisDTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.paisDTitle.Location = new System.Drawing.Point(768, 464);
            this.paisDTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.paisDTitle.Name = "paisDTitle";
            this.paisDTitle.Size = new System.Drawing.Size(49, 22);
            this.paisDTitle.TabIndex = 81;
            this.paisDTitle.Text = "País:";
            this.paisDTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // numeroText
            // 
            this.numeroText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numeroText.Location = new System.Drawing.Point(115, 513);
            this.numeroText.Name = "numeroText";
            this.numeroText.Size = new System.Drawing.Size(108, 31);
            this.numeroText.TabIndex = 80;
            // 
            // numeroTitle
            // 
            this.numeroTitle.AutoSize = true;
            this.numeroTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.numeroTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numeroTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.numeroTitle.Location = new System.Drawing.Point(17, 519);
            this.numeroTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.numeroTitle.Name = "numeroTitle";
            this.numeroTitle.Size = new System.Drawing.Size(88, 22);
            this.numeroTitle.TabIndex = 79;
            this.numeroTitle.Text = "Número:";
            this.numeroTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // calleText
            // 
            this.calleText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calleText.Location = new System.Drawing.Point(115, 461);
            this.calleText.Name = "calleText";
            this.calleText.Size = new System.Drawing.Size(258, 31);
            this.calleText.TabIndex = 78;
            // 
            // calleTitle
            // 
            this.calleTitle.AutoSize = true;
            this.calleTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.calleTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calleTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.calleTitle.Location = new System.Drawing.Point(17, 464);
            this.calleTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.calleTitle.Name = "calleTitle";
            this.calleTitle.Size = new System.Drawing.Size(61, 22);
            this.calleTitle.TabIndex = 77;
            this.calleTitle.Text = "Calle:";
            this.calleTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // direccionTitle
            // 
            this.direccionTitle.AutoSize = true;
            this.direccionTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.direccionTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.direccionTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.direccionTitle.Location = new System.Drawing.Point(17, 426);
            this.direccionTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.direccionTitle.Name = "direccionTitle";
            this.direccionTitle.Size = new System.Drawing.Size(98, 23);
            this.direccionTitle.TabIndex = 76;
            this.direccionTitle.Text = "Dirección";
            this.direccionTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // datosTitle
            // 
            this.datosTitle.AutoSize = true;
            this.datosTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.datosTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.datosTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.datosTitle.Location = new System.Drawing.Point(17, 571);
            this.datosTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.datosTitle.Name = "datosTitle";
            this.datosTitle.Size = new System.Drawing.Size(279, 23);
            this.datosTitle.TabIndex = 91;
            this.datosTitle.Text = "Datos resposable del alumno";
            this.datosTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // seguroText
            // 
            this.seguroText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.seguroText.Location = new System.Drawing.Point(933, 151);
            this.seguroText.Name = "seguroText";
            this.seguroText.Size = new System.Drawing.Size(198, 31);
            this.seguroText.TabIndex = 93;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label2.Location = new System.Drawing.Point(771, 154);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(150, 22);
            this.label2.TabIndex = 92;
            this.label2.Text = "Cédula seguro:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // direccionRText
            // 
            this.direccionRText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.direccionRText.Location = new System.Drawing.Point(133, 674);
            this.direccionRText.Name = "direccionRText";
            this.direccionRText.Size = new System.Drawing.Size(440, 31);
            this.direccionRText.TabIndex = 101;
            this.direccionRText.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label4.Location = new System.Drawing.Point(17, 677);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(101, 22);
            this.label4.TabIndex = 100;
            this.label4.Text = "Dirección:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Telefono1Text
            // 
            this.Telefono1Text.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Telefono1Text.Location = new System.Drawing.Point(159, 728);
            this.Telefono1Text.Name = "Telefono1Text";
            this.Telefono1Text.Size = new System.Drawing.Size(244, 31);
            this.Telefono1Text.TabIndex = 99;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label5.Location = new System.Drawing.Point(17, 731);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(125, 22);
            this.label5.TabIndex = 98;
            this.label5.Text = "1er teléfono:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // claveRText
            // 
            this.claveRText.Enabled = false;
            this.claveRText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.claveRText.Location = new System.Drawing.Point(102, 619);
            this.claveRText.Name = "claveRText";
            this.claveRText.Size = new System.Drawing.Size(108, 31);
            this.claveRText.TabIndex = 97;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label7.Location = new System.Drawing.Point(17, 622);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 22);
            this.label7.TabIndex = 96;
            this.label7.Text = "Clave:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label8.Location = new System.Drawing.Point(771, 212);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(136, 22);
            this.label8.TabIndex = 94;
            this.label8.Text = "Siglas carrera:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // nombreRText
            // 
            this.nombreRText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nombreRText.Location = new System.Drawing.Point(330, 619);
            this.nombreRText.Name = "nombreRText";
            this.nombreRText.Size = new System.Drawing.Size(360, 31);
            this.nombreRText.TabIndex = 103;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label9.Location = new System.Drawing.Point(225, 622);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(89, 22);
            this.label9.TabIndex = 102;
            this.label9.Text = "Nombre:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // telefono2Text
            // 
            this.telefono2Text.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.telefono2Text.Location = new System.Drawing.Point(573, 728);
            this.telefono2Text.Name = "telefono2Text";
            this.telefono2Text.Size = new System.Drawing.Size(244, 31);
            this.telefono2Text.TabIndex = 105;
            this.telefono2Text.TextChanged += new System.EventHandler(this.textBox8_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label10.Location = new System.Drawing.Point(431, 731);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(132, 22);
            this.label10.TabIndex = 104;
            this.label10.Text = "2do teléfono:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label10.Click += new System.EventHandler(this.label10_Click);
            // 
            // mail2Text
            // 
            this.mail2Text.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mail2Text.Location = new System.Drawing.Point(719, 674);
            this.mail2Text.Name = "mail2Text";
            this.mail2Text.Size = new System.Drawing.Size(244, 31);
            this.mail2Text.TabIndex = 107;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label11.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label11.Location = new System.Drawing.Point(604, 677);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(110, 22);
            this.label11.TabIndex = 106;
            this.label11.Text = "2do e-mail:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // mail1Text
            // 
            this.mail1Text.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mail1Text.Location = new System.Drawing.Point(827, 619);
            this.mail1Text.Name = "mail1Text";
            this.mail1Text.Size = new System.Drawing.Size(244, 31);
            this.mail1Text.TabIndex = 109;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label12.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label12.Location = new System.Drawing.Point(712, 622);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(103, 22);
            this.label12.TabIndex = 108;
            this.label12.Text = "1er e-mail:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // carreraText
            // 
            this.carreraText.FormattingEnabled = true;
            this.carreraText.Location = new System.Drawing.Point(933, 209);
            this.carreraText.Name = "carreraText";
            this.carreraText.Size = new System.Drawing.Size(198, 30);
            this.carreraText.TabIndex = 110;
            // 
            // regresarBoton
            // 
            this.regresarBoton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(109)))), ((int)(((byte)(188)))));
            this.regresarBoton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.regresarBoton.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.regresarBoton.ForeColor = System.Drawing.Color.White;
            this.regresarBoton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.regresarBoton.Location = new System.Drawing.Point(983, 735);
            this.regresarBoton.Name = "regresarBoton";
            this.regresarBoton.Size = new System.Drawing.Size(148, 41);
            this.regresarBoton.TabIndex = 112;
            this.regresarBoton.Text = "Regresar";
            this.regresarBoton.UseVisualStyleBackColor = false;
            this.regresarBoton.Click += new System.EventHandler(this.regresarBoton_Click);
            // 
            // ejecutarBoton
            // 
            this.ejecutarBoton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(109)))), ((int)(((byte)(188)))));
            this.ejecutarBoton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ejecutarBoton.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ejecutarBoton.ForeColor = System.Drawing.Color.White;
            this.ejecutarBoton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ejecutarBoton.Location = new System.Drawing.Point(983, 668);
            this.ejecutarBoton.Name = "ejecutarBoton";
            this.ejecutarBoton.Size = new System.Drawing.Size(148, 41);
            this.ejecutarBoton.TabIndex = 111;
            this.ejecutarBoton.Text = "Ejecutar";
            this.ejecutarBoton.UseVisualStyleBackColor = false;
            this.ejecutarBoton.Click += new System.EventHandler(this.ejecutarBoton_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label13.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label13.Location = new System.Drawing.Point(356, 257);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(99, 22);
            this.label13.TabIndex = 113;
            this.label13.Text = "Teléfonos:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // telefonosText
            // 
            this.telefonosText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.telefonosText.Location = new System.Drawing.Point(479, 254);
            this.telefonosText.Name = "telefonosText";
            this.telefonosText.Size = new System.Drawing.Size(269, 31);
            this.telefonosText.TabIndex = 114;
            // 
            // AltaAlumno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1159, 791);
            this.Controls.Add(this.telefonosText);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.regresarBoton);
            this.Controls.Add(this.ejecutarBoton);
            this.Controls.Add(this.carreraText);
            this.Controls.Add(this.mail1Text);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.mail2Text);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.telefono2Text);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.nombreRText);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.direccionRText);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Telefono1Text);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.claveRText);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.seguroText);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.datosTitle);
            this.Controls.Add(this.ciudadText);
            this.Controls.Add(this.ciudadTitle);
            this.Controls.Add(this.coloniaText);
            this.Controls.Add(this.coloniaTitle);
            this.Controls.Add(this.cpText);
            this.Controls.Add(this.cpTitle);
            this.Controls.Add(this.estadoDText);
            this.Controls.Add(this.estadoDTitle);
            this.Controls.Add(this.paisDText);
            this.Controls.Add(this.paisDTitle);
            this.Controls.Add(this.numeroText);
            this.Controls.Add(this.numeroTitle);
            this.Controls.Add(this.calleText);
            this.Controls.Add(this.calleTitle);
            this.Controls.Add(this.direccionTitle);
            this.Controls.Add(this.nacionalidadText);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.anoText);
            this.Controls.Add(this.anoTitle);
            this.Controls.Add(this.estadoNText);
            this.Controls.Add(this.estadoNTitle);
            this.Controls.Add(this.paisNText);
            this.Controls.Add(this.paisNTitle);
            this.Controls.Add(this.mesText);
            this.Controls.Add(this.mesTitle);
            this.Controls.Add(this.diaText);
            this.Controls.Add(this.diaTitle);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.apellidoMText);
            this.Controls.Add(this.apellidoMTitle);
            this.Controls.Add(this.apellidoPText);
            this.Controls.Add(this.apellidoPTitle);
            this.Controls.Add(this.nombreText);
            this.Controls.Add(this.nombreTitle);
            this.Controls.Add(this.matriculaText);
            this.Controls.Add(this.nominaTitle);
            this.Controls.Add(this.LaberQuerie);
            this.Controls.Add(this.pictureBox1);
            this.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Name = "AltaAlumno";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "e";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LaberQuerie;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox anoText;
        private System.Windows.Forms.Label anoTitle;
        private System.Windows.Forms.TextBox estadoNText;
        private System.Windows.Forms.Label estadoNTitle;
        private System.Windows.Forms.TextBox paisNText;
        private System.Windows.Forms.Label paisNTitle;
        private System.Windows.Forms.TextBox mesText;
        private System.Windows.Forms.Label mesTitle;
        private System.Windows.Forms.TextBox diaText;
        private System.Windows.Forms.Label diaTitle;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox apellidoMText;
        private System.Windows.Forms.Label apellidoMTitle;
        private System.Windows.Forms.TextBox apellidoPText;
        private System.Windows.Forms.Label apellidoPTitle;
        private System.Windows.Forms.TextBox nombreText;
        private System.Windows.Forms.Label nombreTitle;
        private System.Windows.Forms.TextBox matriculaText;
        private System.Windows.Forms.Label nominaTitle;
        private System.Windows.Forms.TextBox nacionalidadText;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox ciudadText;
        private System.Windows.Forms.Label ciudadTitle;
        private System.Windows.Forms.TextBox coloniaText;
        private System.Windows.Forms.Label coloniaTitle;
        private System.Windows.Forms.TextBox cpText;
        private System.Windows.Forms.Label cpTitle;
        private System.Windows.Forms.TextBox estadoDText;
        private System.Windows.Forms.Label estadoDTitle;
        private System.Windows.Forms.TextBox paisDText;
        private System.Windows.Forms.Label paisDTitle;
        private System.Windows.Forms.TextBox numeroText;
        private System.Windows.Forms.Label numeroTitle;
        private System.Windows.Forms.TextBox calleText;
        private System.Windows.Forms.Label calleTitle;
        private System.Windows.Forms.Label direccionTitle;
        private System.Windows.Forms.Label datosTitle;
        private System.Windows.Forms.TextBox seguroText;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox direccionRText;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox Telefono1Text;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox claveRText;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox nombreRText;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox telefono2Text;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox mail2Text;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox mail1Text;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox carreraText;
        private System.Windows.Forms.Button regresarBoton;
        private System.Windows.Forms.Button ejecutarBoton;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox telefonosText;
    }
}