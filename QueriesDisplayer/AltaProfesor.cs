﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.InteropServices;



namespace QueriesDisplayer
{
    public partial class AltaProfesor : Form
    {
        // Metodo para obtener las posiciones de las esquinas del windows form
        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        private static extern IntPtr CreateRoundRectRgn
        (
            int nLeftRect,     // x-coordinate of upper-left corner
            int nTopRect,      // y-coordinate of upper-left corner
            int nRightRect,    // x-coordinate of lower-right corner
            int nBottomRect,   // y-coordinate of lower-right corner
            int nWidthEllipse, // height of ellipse
            int nHeightEllipse // width of ellipse
        );

        public AltaProfesor()
        {
            InitializeComponent();

            // Asignacion de esquinas redondeadas
            this.FormBorderStyle = FormBorderStyle.None;
            Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, Width, Height, 20, 20));

            // Borrado de campos
            InitializeComponent();
            nominaText.Text = "";
            nombreText.Text = "";
            apellidoPText.Text = "";
            apellidoMText.Text = "";
            rfcText.Text = "";
            diaText.Text = "";
            mesText.Text = "";
            anoText.Text = "";
            paisNText.Text = "";
            estadoNText.Text = "";
            calleText.Text = "";
            numeroText.Text = "";
            coloniaText.Text = "";
            ciudadText.Text = "";
            estadoDText.Text = "";
            paisDText.Text = "";
            cpText.Text = "";
            maestriaText.Text = "";
            doctoradoText.Text = "";
            categoriaText.Text = "";
            cipText.Text = "";
            telefonoText.Text = "";
            emailText.Text = "";
            nacionalidadText.Text = "";

            // Declaracion de variables para las consultas
            string connetionString;
            SqlConnection connection;
            SqlCommand command;
            SqlDataReader dataReader;
            string output = "";
            string query = "";

            connetionString = "Data Source=ccastro.database.windows.net;Initial Catalog=DataBaseOnline;User ID=carloscastro;Password=Ultimate45$";
            connection = new SqlConnection(connetionString);
            connection.Open();
            query = "SELECT ClaveCIP FROM CIP";
            command = new SqlCommand(query, connection);
            dataReader = command.ExecuteReader();
            while (dataReader.Read())
            {
                output = dataReader.GetValue(0).ToString();
                cipText.Items.Add(output);
            }
            dataReader.Close();
            connection.Close();

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (this.maestriaTitle.Checked)
            {
                this.maestriaText.Visible = true;
            }
            else
            {
                this.maestriaText.Visible = false;
                this.maestriaText.Text = "";

            }
        }

        private void doctoradoTitle_CheckedChanged(object sender, EventArgs e)
        {
            if (this.doctoradoTitle.Checked)
            {
                this.doctoradoText.Visible = true;
            }
            else
            {
                this.doctoradoText.Visible = false ;
                this.doctoradoText.Text = "";

            }
        }

        private void regresarBoton_Click(object sender, EventArgs e)
        {
            this.Hide();
            QueriesDisplayer inicio = new QueriesDisplayer();
            inicio.Show();
        }

        private void categoriaText_SelectedIndexChanged(object sender, EventArgs e)
        {
           
            
        }

        // Funcion encargada de checar que los campos esten completos, es decir, que contengan texto o 
        // datos para ingresar.
        private bool EntradaCompleta()
        {
            if (nominaText.Text == "" && nombreText.Text == "" && apellidoPText.Text == "" && apellidoMText.Text == ""
                && rfcText.Text == "" && diaText.Text == "" && mesText.Text == "" && anoText.Text == "" && paisNText.Text == ""
                && estadoNText.Text == "" && calleText.Text == "" && numeroText.Text == "" && coloniaText.Text == ""
                && ciudadText.Text == "" && estadoDText.Text == "" && paisDText.Text == "" && cpText.Text == ""
                && categoriaText.Text == "" && cipText.Text == "" && telefonoText.Text=="" && emailText.Text=="" && nacionalidadText.Text=="")
            {
                return false;
            }
            return true;
        }

        private void ejecutarBoton_Click(object sender, EventArgs e)
        {
            if (EntradaCompleta())
            {
                //string query;
                //string connetionString;
                //SqlConnection connection;
                //SqlDataAdapter command;
                //DataTable table;

                //query = "execute alta_profesor '" + nominaText + "', '" + apellidoPText + "', '" + apellidoMText + "', " + anoText + ", '" + mesText + "', '" + diaText + "', '" +
                //         estadoNText + "', '" + paisNText + "', '" + rfcText + "', '" + calleText + "', '" + "', '" + numeroText + "', '" + cpText + "', '" + estadoDText + "', '" + paisDText +
                //         "', '" + ciudadText + "', '" + coloniaText + "', '" + maestriaText + "', '" + doctoradoText + "', '" + categoriaText + "'"; connetionString = "Data Source=ccastro.database.windows.net;Initial Catalog=DataBaseOnline;User ID=carloscastro;Password=Ultimate45$";

                //connection = new SqlConnection(connetionString);
                //try
                //{
                //    connection.Open();
                //    command = new SqlDataAdapter(query, connection);
                   
                //    command.Dispose();
                //    connection.Close();


                //}
                //catch (System.Data.SqlClient.SqlException ex)
                //{
                //    MessageBox.Show(ex.Message + " !");
                //}

            }
            else
            {
                MessageBox.Show("No se pueden dejar entradas vacias", "Bases de datos TEC", MessageBoxButtons.OK);
            }
        }

        private void label2_Click_1(object sender, EventArgs e)
        {

        }
    }
}
