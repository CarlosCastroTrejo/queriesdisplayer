﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.InteropServices;



namespace QueriesDisplayer
{
    public partial class AltaGrupo : Form
    {

        // Metodo para obtener las posiciones de las esquinas del windows form
        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        private static extern IntPtr CreateRoundRectRgn
        (
            int nLeftRect,     // x-coordinate of upper-left corner
            int nTopRect,      // y-coordinate of upper-left corner
            int nRightRect,    // x-coordinate of lower-right corner
            int nBottomRect,   // y-coordinate of lower-right corner
            int nWidthEllipse, // height of ellipse
            int nHeightEllipse // width of ellipse
        );

        public AltaGrupo()
        {
            InitializeComponent();

            // Asignacion de esquinas redondeadas
            this.FormBorderStyle = FormBorderStyle.None;
            Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, Width, Height, 20, 20));


            // Modificacion del formato del date picker para que solo elija el anio
            anoPeriodo.Format = DateTimePickerFormat.Custom;
            anoPeriodo.CustomFormat = "yyyy";
            anoPeriodo.ShowUpDown = true;

            // Borrado de campos
            claveText.Text = "";
            periodoText.Text = "";
            anoPeriodo.Text = "";
            numeroGText.Text = "";
            calif1Text.Text = "";
            calif2Text.Text = "";
            salonText.Text = "";
            materiaText.Text = "";
            horarioText.Text = "";
            profesorText.Text = "";

            // Declaracion de variables para hacer queries
            string connetionString;
            SqlConnection connection;
            string output = "";
            string query = "";
            SqlCommand command;
            SqlDataReader dataReader;
            connetionString = "Data Source=ccastro.database.windows.net;Initial Catalog=DataBaseOnline;User ID=carloscastro;Password=Ultimate45$";
            connection = new SqlConnection(connetionString);


            // Query para asignar clave al responsable del alumno
            // Hacemos un try catch por si no hay ningun grupo la clave sera 0
            // de lo contrario sera 1 mas del grupo anterios
            try
            {
                connection.Open();
                query = "select ClaveGrupo from Grupo";
                command = new SqlCommand(query, connection);
                dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    output = dataReader.GetValue(0).ToString();
                }
                int clave = Convert.ToInt32(output);
                clave++;
                claveText.Text = clave.ToString();
                dataReader.Close();
                connection.Close();
            }
            catch (Exception ex)
            {
                claveText.Text = "0";
                connection.Close();
            }
            

            // Query para obtener los salones y asignarlos al combobox
            connection.Open();
            query = "SELECT ClaveSalon FROM Salon";
            command = new SqlCommand(query, connection);
            dataReader = command.ExecuteReader();
            while (dataReader.Read())
            {
                output = dataReader.GetValue(0).ToString();
                salonText.Items.Add(output);
            }
            dataReader.Close();
            connection.Close();

            // Query para obtener las materias y asignarlos al combobox
            connection.Open();
            query = "SELECT ClaveMateria FROM Materia";
            command = new SqlCommand(query, connection);
            dataReader = command.ExecuteReader();
            while (dataReader.Read())
            {
                output = dataReader.GetValue(0).ToString();
                materiaText.Items.Add(output);
            }
            dataReader.Close();
            connection.Close();

            // Query para obtener los horarios y asignarlos al combobox
            connection.Open();
            query = "SELECT ClaveHorario FROM Horario";
            command = new SqlCommand(query, connection);
            dataReader = command.ExecuteReader();
            while (dataReader.Read())
            {
                output = dataReader.GetValue(0).ToString();
                horarioText.Items.Add(output);
            }
            dataReader.Close();
            connection.Close();

            // Query para obtener los profesores y asignarlos al combobox
            connection.Open();
            query = "SELECT NominaProfesor FROM Profesor";
            command = new SqlCommand(query, connection);
            dataReader = command.ExecuteReader();
            while (dataReader.Read())
            {
                output = dataReader.GetValue(0).ToString();
                profesorText.Items.Add(output);
            }
            dataReader.Close();
            connection.Close();

        }

        private void AltaGrupo_Load(object sender, EventArgs e)
        {

        }


        // Funcion encargada de checar que los campos esten completos, es decir, que contengan texto o 
        // datos para ingresar.
        private bool EntradaCompleta()
        {
            if (claveText.Text == "" && periodoText.Text=="" && anoPeriodo.Text=="" && numeroGText.Text=="" &&
                calif1Text.Text == ""  && calif2Text.Text=="" && salonText.Text=="" && materiaText.Text=="" 
                && horarioText.Text=="" && profesorText.Text=="" )
            {
                return false;
            }
            return true;
        }


        private void regresarBoton_Click(object sender, EventArgs e)
        {
            this.Hide();
            QueriesDisplayer inico = new QueriesDisplayer();
            inico.Show();
        }

        private void ejecutarBoton_Click(object sender, EventArgs e)
        {
            if (EntradaCompleta())
            {

            }
            else
            {
                MessageBox.Show("No se pueden dejar entradas vacias", "Bases de datos TEC", MessageBoxButtons.OK);
            }
        }
    }
}
