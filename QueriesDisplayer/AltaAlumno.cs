﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.InteropServices;



namespace QueriesDisplayer
{
    public partial class AltaAlumno : Form
    {
        // Metodo para obtener las posiciones de las esquinas del windows form
        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        private static extern IntPtr CreateRoundRectRgn
        (
            int nLeftRect,     // x-coordinate of upper-left corner
            int nTopRect,      // y-coordinate of upper-left corner
            int nRightRect,    // x-coordinate of lower-right corner
            int nBottomRect,   // y-coordinate of lower-right corner
            int nWidthEllipse, // height of ellipse
            int nHeightEllipse // width of ellipse
        );


        public AltaAlumno()
        {
            InitializeComponent();

            // Asignacion de esquinas redondeadas
            this.FormBorderStyle = FormBorderStyle.None;
            Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, Width, Height, 20, 20));

            // Borrado de campos
            InitializeComponent();
            matriculaText.Text = "";
            nombreText.Text = "";
            apellidoPText.Text = "";
            apellidoMText.Text = "";
            seguroText.Text = "";
            carreraText.Text = "";
            telefonosText.Text = "";
            diaText.Text = "";
            mesText.Text = "";
            anoText.Text = "";
            paisNText.Text = "";
            estadoNText.Text = "";
            nacionalidadText.Text = "";
            calleText.Text = "";
            numeroText.Text = "";
            ciudadText.Text = "";
            coloniaText.Text = "";
            estadoDText.Text = "";
            paisDText.Text = "";
            cpText.Text = "";
            claveRText.Text = "";
            nombreRText.Text = "";
            direccionRText.Text = "";
            Telefono1Text.Text = "";
            telefono2Text.Text = "";
            mail1Text.Text = "";
            mail2Text.Text = "";

        

            // Declaracion de variables para hacer queries
            string connetionString;
            SqlConnection connection;
            string output = "";
            string query;
            SqlCommand command;
            SqlDataReader dataReader;
            connetionString = "Data Source=ccastro.database.windows.net;Initial Catalog=DataBaseOnline;User ID=carloscastro;Password=Ultimate45$";
            connection = new SqlConnection(connetionString);


            // Query para obtener las Siglas de las carreras y asignarlas al comboBox
            connection.Open();
            query = "SELECT SiglasCarrera FROM Carrera";
            command = new SqlCommand(query, connection);
            dataReader = command.ExecuteReader();
            while (dataReader.Read())
            {
                output = dataReader.GetValue(0).ToString();
                carreraText.Items.Add(output);
            }
            dataReader.Close();
            connection.Close();



            // Query para asignar clave al responsable del alumno
            try
            {
                connection.Open();
                query = "select ClaveResponsable from ResponsableColegiatura";
                command= new SqlCommand(query, connection);
                dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    output = dataReader.GetValue(0).ToString();
                }
                int clave = Convert.ToInt32(output);
                clave++;
                claveRText.Text = clave.ToString();
                dataReader.Close();
                connection.Close();
            }
            catch(Exception ex)
            {
                claveRText.Text = "0";
            }
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void textBox8_TextChanged(object sender, EventArgs e)
        {

        }

        // Funcion encargada de checar que los campos esten completos, es decir, que contengan texto o 
        // datos para ingresar.
        private bool EntradaCompleta()
        {
            if (matriculaText.Text == "" && nombreText.Text == "" && apellidoPText.Text == "" && apellidoMText.Text == ""
                && seguroText.Text == "" && carreraText.Text=="" && telefonosText.Text=="" && diaText.Text == "" && 
                mesText.Text == "" && anoText.Text == "" && paisNText.Text == "" && estadoNText.Text == "" && calleText.Text == "" 
                && numeroText.Text == "" && coloniaText.Text == "" && ciudadText.Text == "" && estadoDText.Text == "" 
                && paisDText.Text == "" && cpText.Text == "" && claveRText.Text == "" && nombreRText.Text == "" 
                && direccionRText.Text == "" && Telefono1Text.Text == ""  && telefono2Text.Text==""  && nacionalidadText.Text == "" &&
                mail1Text.Text=="" && mail2Text.Text=="")
            {
                return false;
            }
            return true;
        }


        private void regresarBoton_Click(object sender, EventArgs e)
        {
            this.Hide();
            QueriesDisplayer inicio = new QueriesDisplayer();
            inicio.Show();
        }

        private void ejecutarBoton_Click(object sender, EventArgs e)
        {
            if (EntradaCompleta())
            {
                //string query;
                //string connetionString;
                //SqlConnection connection;
                //SqlDataAdapter command;
                //DataTable table;

                //query = "execute alta_profesor '" + nominaText + "', '" + apellidoPText + "', '" + apellidoMText + "', '" + anoText + "', '" + mesText + "', '" + diaText + "', '" +
                //         estadoNText + "', '" + paisNText + "', '" + rfcText + "', '" + calleText + "', '" + "', '" + numeroText + "', '" + cpText + "', '" + estadoDText + "', '" + paisDText +
                //         "', '" + ciudadText + "', '" + coloniaText + "', '" + maestriaText + "', '" + doctoradoText + "', '" + categoriaText + "'"; connetionString = "Data Source=ccastro.database.windows.net;Initial Catalog=DataBaseOnline;User ID=carloscastro;Password=Ultimate45$";

                //connection = new SqlConnection(connetionString);
                //try
                //{
                //    connection.Open();
                //    command = new SqlDataAdapter(query, connection);

                //    command.Dispose();
                //    connection.Close();


                //}
                //catch (System.Data.SqlClient.SqlException ex)
                //{
                //    MessageBox.Show(ex.Message + " !");
                //}
            }
            else
            {
                MessageBox.Show("No se pueden dejar entradas vacias", "Bases de datos TEC", MessageBoxButtons.OK);
            }
        }

        private void matriculaText_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
