﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.InteropServices;


namespace QueriesDisplayer
{
    public partial class AltaMateria : Form
    {

        // Metodo para obtener las posiciones de las esquinas del windows form
        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        private static extern IntPtr CreateRoundRectRgn
        (
            int nLeftRect,     // x-coordinate of upper-left corner
            int nTopRect,      // y-coordinate of upper-left corner
            int nRightRect,    // x-coordinate of lower-right corner
            int nBottomRect,   // y-coordinate of lower-right corner
            int nWidthEllipse, // height of ellipse
            int nHeightEllipse // width of ellipse
        );

        public AltaMateria()
        {
            InitializeComponent();
            // Asignacion de esquinas redondeadas
            this.FormBorderStyle = FormBorderStyle.None;
            Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, Width, Height, 20, 20));

            // Borrado de campos
            claveText.Text = "";
            nombreText.Text = "";
            abrevText.Text = "";
            carreraText.Text = "";
            unidadesText.Text = "";
            cipText.Text = "";
            departamentoText.Text = "";


            // Declaracion de variables para las consultas
            string connetionString;
            SqlConnection connection;
            SqlCommand command;
            SqlDataReader dataReader;
            string output = "";
            string query = "";
            connetionString = "Data Source=ccastro.database.windows.net;Initial Catalog=DataBaseOnline;User ID=carloscastro;Password=Ultimate45$";
            connection = new SqlConnection(connetionString);

            // Consulta para obtenr las claves CIP y asignarlas al comboBox
            connection.Open();
            query = "SELECT ClaveCIP FROM CIP";
            command = new SqlCommand(query, connection);
            dataReader = command.ExecuteReader();
            while (dataReader.Read())
            {
                output = dataReader.GetValue(0).ToString();
                cipText.Items.Add(output);
            }
            dataReader.Close();
            connection.Close();

            // Consulta para obtener las siglas de todas las carreras y asignarlas al comboBx
            connection.Open();
            query = "SELECT SiglasCarrera FROM Carrera";
            command = new SqlCommand(query, connection);
            dataReader = command.ExecuteReader();
            while (dataReader.Read())
            {
                output = dataReader.GetValue(0).ToString();
                carreraText.Items.Add(output);
            }
            dataReader.Close();
            connection.Close();

            // Consulta para obtener las siglas de todos los departamentos y asignarlas al comboBx
            connection.Open();
            query = "SELECT SiglasDepartamento FROM Departamento";
            command = new SqlCommand(query, connection);
            dataReader = command.ExecuteReader();
            while (dataReader.Read())
            {
                output = dataReader.GetValue(0).ToString();
                departamentoText.Items.Add(output);
            }
            dataReader.Close();
            connection.Close();
        }

        private void LaberQuerie_Click(object sender, EventArgs e)
        {

        }

        private void AltaMateria_Load(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void unidadesText_TextChanged(object sender, EventArgs e)
        {

        }

        // Funcion encargada de checar que los campos esten completos, es decir, que contengan texto o 
        // datos para ingresar.
        private bool EntradaCompleta()
        {
            if (claveText.Text == "" && nombreText.Text == "" && abrevText.Text == "" && carreraText.Text == "" && unidadesText.Text == ""
                && cipText.Text == "" && departamentoText.Text == "" && temasText.Text=="")
            {
                return false;
            }
            return true;
        }
        
        private void regresarBoton_Click(object sender, EventArgs e)
        {
            this.Hide();
            QueriesDisplayer inicio = new QueriesDisplayer();
            inicio.Show();
        }

        private void ejecutarBoton_Click(object sender, EventArgs e)
        {
            if (EntradaCompleta())
            {

            }
            else
            {
                MessageBox.Show("No se pueden dejar entradas vacias", "Bases de datos TEC", MessageBoxButtons.OK);
            }

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void cipText_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void claveText_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
