﻿namespace QueriesDisplayer
{
    partial class AltaGrupo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AltaGrupo));
            this.label9 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.claveText = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.numeroGText = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.calif1Text = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.calif2Text = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.regresarBoton = new System.Windows.Forms.Button();
            this.ejecutarBoton = new System.Windows.Forms.Button();
            this.anoPeriodo = new System.Windows.Forms.DateTimePicker();
            this.periodoText = new System.Windows.Forms.ComboBox();
            this.horarioText = new System.Windows.Forms.ComboBox();
            this.materiaText = new System.Windows.Forms.ComboBox();
            this.salonText = new System.Windows.Forms.ComboBox();
            this.profesorText = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 24F);
            this.label9.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label9.Location = new System.Drawing.Point(253, 30);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(496, 39);
            this.label9.TabIndex = 119;
            this.label9.Text = "Base de datos TEC - Alta grupo";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label2.Location = new System.Drawing.Point(275, 165);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 22);
            this.label2.TabIndex = 117;
            this.label2.Text = "Periodo:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // claveText
            // 
            this.claveText.Enabled = false;
            this.claveText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.claveText.Location = new System.Drawing.Point(110, 162);
            this.claveText.Name = "claveText";
            this.claveText.Size = new System.Drawing.Size(124, 31);
            this.claveText.TabIndex = 116;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label7.Location = new System.Drawing.Point(28, 165);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 22);
            this.label7.TabIndex = 115;
            this.label7.Text = "Clave:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label6.Location = new System.Drawing.Point(28, 122);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(189, 23);
            this.label6.TabIndex = 114;
            this.label6.Text = "Información básica";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(109)))), ((int)(((byte)(188)))));
            this.pictureBox1.ErrorImage = null;
            this.pictureBox1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1047, 100);
            this.pictureBox1.TabIndex = 113;
            this.pictureBox1.TabStop = false;
            // 
            // numeroGText
            // 
            this.numeroGText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numeroGText.Location = new System.Drawing.Point(902, 162);
            this.numeroGText.Name = "numeroGText";
            this.numeroGText.Size = new System.Drawing.Size(111, 31);
            this.numeroGText.TabIndex = 121;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label1.Location = new System.Drawing.Point(703, 165);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(179, 22);
            this.label1.TabIndex = 120;
            this.label1.Text = "Número de grupo:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // calif1Text
            // 
            this.calif1Text.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calif1Text.Location = new System.Drawing.Point(244, 224);
            this.calif1Text.Name = "calif1Text";
            this.calif1Text.Size = new System.Drawing.Size(198, 31);
            this.calif1Text.TabIndex = 123;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label3.Location = new System.Drawing.Point(28, 227);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(206, 22);
            this.label3.TabIndex = 122;
            this.label3.Text = "Calificación parcial 1:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // calif2Text
            // 
            this.calif2Text.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calif2Text.Location = new System.Drawing.Point(683, 224);
            this.calif2Text.Name = "calif2Text";
            this.calif2Text.Size = new System.Drawing.Size(198, 31);
            this.calif2Text.TabIndex = 125;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label4.Location = new System.Drawing.Point(467, 227);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(206, 22);
            this.label4.TabIndex = 124;
            this.label4.Text = "Calificación parcial 2:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label5.Location = new System.Drawing.Point(28, 279);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 22);
            this.label5.TabIndex = 126;
            this.label5.Text = "Salón:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label8.Location = new System.Drawing.Point(335, 279);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(86, 22);
            this.label8.TabIndex = 128;
            this.label8.Text = "Materia:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label10.Location = new System.Drawing.Point(28, 333);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(87, 22);
            this.label10.TabIndex = 130;
            this.label10.Text = "Profesor:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label11.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label11.Location = new System.Drawing.Point(696, 279);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(80, 22);
            this.label11.TabIndex = 132;
            this.label11.Text = "Horario:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // regresarBoton
            // 
            this.regresarBoton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(109)))), ((int)(((byte)(188)))));
            this.regresarBoton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.regresarBoton.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.regresarBoton.ForeColor = System.Drawing.Color.White;
            this.regresarBoton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.regresarBoton.Location = new System.Drawing.Point(851, 445);
            this.regresarBoton.Name = "regresarBoton";
            this.regresarBoton.Size = new System.Drawing.Size(148, 41);
            this.regresarBoton.TabIndex = 137;
            this.regresarBoton.Text = "Regresar";
            this.regresarBoton.UseVisualStyleBackColor = false;
            this.regresarBoton.Click += new System.EventHandler(this.regresarBoton_Click);
            // 
            // ejecutarBoton
            // 
            this.ejecutarBoton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(109)))), ((int)(((byte)(188)))));
            this.ejecutarBoton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ejecutarBoton.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ejecutarBoton.ForeColor = System.Drawing.Color.White;
            this.ejecutarBoton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ejecutarBoton.Location = new System.Drawing.Point(851, 378);
            this.ejecutarBoton.Name = "ejecutarBoton";
            this.ejecutarBoton.Size = new System.Drawing.Size(148, 41);
            this.ejecutarBoton.TabIndex = 136;
            this.ejecutarBoton.Text = "Ejecutar";
            this.ejecutarBoton.UseVisualStyleBackColor = false;
            this.ejecutarBoton.Click += new System.EventHandler(this.ejecutarBoton_Click);
            // 
            // anoPeriodo
            // 
            this.anoPeriodo.Location = new System.Drawing.Point(544, 162);
            this.anoPeriodo.Name = "anoPeriodo";
            this.anoPeriodo.Size = new System.Drawing.Size(101, 31);
            this.anoPeriodo.TabIndex = 138;
            // 
            // periodoText
            // 
            this.periodoText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.periodoText.FormattingEnabled = true;
            this.periodoText.Items.AddRange(new object[] {
            "Ene-May",
            "Ago-Dic",
            "Verano"});
            this.periodoText.Location = new System.Drawing.Point(364, 162);
            this.periodoText.Name = "periodoText";
            this.periodoText.Size = new System.Drawing.Size(159, 30);
            this.periodoText.TabIndex = 139;
            // 
            // horarioText
            // 
            this.horarioText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.horarioText.FormattingEnabled = true;
            this.horarioText.Location = new System.Drawing.Point(781, 276);
            this.horarioText.Name = "horarioText";
            this.horarioText.Size = new System.Drawing.Size(232, 30);
            this.horarioText.TabIndex = 140;
            // 
            // materiaText
            // 
            this.materiaText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.materiaText.FormattingEnabled = true;
            this.materiaText.Location = new System.Drawing.Point(432, 276);
            this.materiaText.Name = "materiaText";
            this.materiaText.Size = new System.Drawing.Size(241, 30);
            this.materiaText.TabIndex = 141;
            // 
            // salonText
            // 
            this.salonText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.salonText.FormattingEnabled = true;
            this.salonText.Location = new System.Drawing.Point(102, 276);
            this.salonText.Name = "salonText";
            this.salonText.Size = new System.Drawing.Size(204, 30);
            this.salonText.TabIndex = 142;
            // 
            // profesorText
            // 
            this.profesorText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.profesorText.FormattingEnabled = true;
            this.profesorText.Location = new System.Drawing.Point(128, 330);
            this.profesorText.Name = "profesorText";
            this.profesorText.Size = new System.Drawing.Size(231, 30);
            this.profesorText.TabIndex = 143;
            // 
            // AltaGrupo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1047, 503);
            this.Controls.Add(this.profesorText);
            this.Controls.Add(this.salonText);
            this.Controls.Add(this.materiaText);
            this.Controls.Add(this.horarioText);
            this.Controls.Add(this.periodoText);
            this.Controls.Add(this.anoPeriodo);
            this.Controls.Add(this.regresarBoton);
            this.Controls.Add(this.ejecutarBoton);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.calif2Text);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.calif1Text);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.numeroGText);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.claveText);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.pictureBox1);
            this.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Name = "AltaGrupo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AltaGrupo";
            this.Load += new System.EventHandler(this.AltaGrupo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox claveText;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox numeroGText;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox calif1Text;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox calif2Text;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button regresarBoton;
        private System.Windows.Forms.Button ejecutarBoton;
        private System.Windows.Forms.DateTimePicker anoPeriodo;
        private System.Windows.Forms.ComboBox periodoText;
        private System.Windows.Forms.ComboBox horarioText;
        private System.Windows.Forms.ComboBox materiaText;
        private System.Windows.Forms.ComboBox salonText;
        private System.Windows.Forms.ComboBox profesorText;
    }
}