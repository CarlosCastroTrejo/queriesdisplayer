﻿namespace QueriesDisplayer
{
    partial class AltaProfesor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AltaProfesor));
            this.LaberQuerie = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.ejecutarBoton = new System.Windows.Forms.Button();
            this.nominaTitle = new System.Windows.Forms.Label();
            this.nominaText = new System.Windows.Forms.TextBox();
            this.nombreText = new System.Windows.Forms.TextBox();
            this.nombreTitle = new System.Windows.Forms.Label();
            this.apellidoPText = new System.Windows.Forms.TextBox();
            this.apellidoPTitle = new System.Windows.Forms.Label();
            this.apellidoMText = new System.Windows.Forms.TextBox();
            this.apellidoMTitle = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.mesText = new System.Windows.Forms.TextBox();
            this.mesTitle = new System.Windows.Forms.Label();
            this.diaText = new System.Windows.Forms.TextBox();
            this.diaTitle = new System.Windows.Forms.Label();
            this.estadoNText = new System.Windows.Forms.TextBox();
            this.estadoNTitle = new System.Windows.Forms.Label();
            this.paisNText = new System.Windows.Forms.TextBox();
            this.paisNTitle = new System.Windows.Forms.Label();
            this.anoText = new System.Windows.Forms.TextBox();
            this.anoTitle = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.rfcText = new System.Windows.Forms.TextBox();
            this.rfcTitle = new System.Windows.Forms.Label();
            this.cpText = new System.Windows.Forms.TextBox();
            this.cpTitle = new System.Windows.Forms.Label();
            this.estadoDText = new System.Windows.Forms.TextBox();
            this.estadoDTitle = new System.Windows.Forms.Label();
            this.paisDText = new System.Windows.Forms.TextBox();
            this.paisDTitle = new System.Windows.Forms.Label();
            this.numeroText = new System.Windows.Forms.TextBox();
            this.numeroTitle = new System.Windows.Forms.Label();
            this.calleText = new System.Windows.Forms.TextBox();
            this.calleTitle = new System.Windows.Forms.Label();
            this.direccionTitle = new System.Windows.Forms.Label();
            this.coloniaText = new System.Windows.Forms.TextBox();
            this.coloniaTitle = new System.Windows.Forms.Label();
            this.ciudadText = new System.Windows.Forms.TextBox();
            this.ciudadTitle = new System.Windows.Forms.Label();
            this.datosTitle = new System.Windows.Forms.Label();
            this.categoriaTitle = new System.Windows.Forms.Label();
            this.categoriaText = new System.Windows.Forms.ComboBox();
            this.maestriaTitle = new System.Windows.Forms.CheckBox();
            this.doctoradoTitle = new System.Windows.Forms.CheckBox();
            this.maestriaText = new System.Windows.Forms.TextBox();
            this.doctoradoText = new System.Windows.Forms.TextBox();
            this.cipTitle = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.regresarBoton = new System.Windows.Forms.Button();
            this.cipText = new System.Windows.Forms.ComboBox();
            this.nacionalidadText = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.emailTitle = new System.Windows.Forms.Label();
            this.telefonoTitle = new System.Windows.Forms.Label();
            this.emailText = new System.Windows.Forms.TextBox();
            this.telefonoText = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // LaberQuerie
            // 
            this.LaberQuerie.AutoSize = true;
            this.LaberQuerie.BackColor = System.Drawing.Color.Transparent;
            this.LaberQuerie.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LaberQuerie.Font = new System.Drawing.Font("Century Gothic", 24F);
            this.LaberQuerie.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.LaberQuerie.Location = new System.Drawing.Point(310, 24);
            this.LaberQuerie.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LaberQuerie.Name = "LaberQuerie";
            this.LaberQuerie.Size = new System.Drawing.Size(529, 39);
            this.LaberQuerie.TabIndex = 13;
            this.LaberQuerie.Text = "Base de datos TEC - Alta profesor";
            this.LaberQuerie.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(109)))), ((int)(((byte)(188)))));
            this.pictureBox1.ErrorImage = null;
            this.pictureBox1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1159, 100);
            this.pictureBox1.TabIndex = 19;
            this.pictureBox1.TabStop = false;
            // 
            // ejecutarBoton
            // 
            this.ejecutarBoton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(109)))), ((int)(((byte)(188)))));
            this.ejecutarBoton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ejecutarBoton.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ejecutarBoton.ForeColor = System.Drawing.Color.White;
            this.ejecutarBoton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ejecutarBoton.Location = new System.Drawing.Point(948, 609);
            this.ejecutarBoton.Name = "ejecutarBoton";
            this.ejecutarBoton.Size = new System.Drawing.Size(148, 41);
            this.ejecutarBoton.TabIndex = 18;
            this.ejecutarBoton.Text = "Ejecutar";
            this.ejecutarBoton.UseVisualStyleBackColor = false;
            this.ejecutarBoton.Click += new System.EventHandler(this.ejecutarBoton_Click);
            // 
            // nominaTitle
            // 
            this.nominaTitle.AutoSize = true;
            this.nominaTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.nominaTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nominaTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.nominaTitle.Location = new System.Drawing.Point(20, 159);
            this.nominaTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.nominaTitle.Name = "nominaTitle";
            this.nominaTitle.Size = new System.Drawing.Size(86, 22);
            this.nominaTitle.TabIndex = 20;
            this.nominaTitle.Text = "Nómina:";
            this.nominaTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.nominaTitle.Click += new System.EventHandler(this.label1_Click);
            // 
            // nominaText
            // 
            this.nominaText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nominaText.Location = new System.Drawing.Point(120, 156);
            this.nominaText.Name = "nominaText";
            this.nominaText.Size = new System.Drawing.Size(231, 31);
            this.nominaText.TabIndex = 21;
            // 
            // nombreText
            // 
            this.nombreText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nombreText.Location = new System.Drawing.Point(120, 211);
            this.nombreText.Name = "nombreText";
            this.nombreText.Size = new System.Drawing.Size(231, 31);
            this.nombreText.TabIndex = 23;
            // 
            // nombreTitle
            // 
            this.nombreTitle.AutoSize = true;
            this.nombreTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.nombreTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nombreTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.nombreTitle.Location = new System.Drawing.Point(20, 214);
            this.nombreTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.nombreTitle.Name = "nombreTitle";
            this.nombreTitle.Size = new System.Drawing.Size(89, 22);
            this.nombreTitle.TabIndex = 22;
            this.nombreTitle.Text = "Nombre:";
            this.nombreTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // apellidoPText
            // 
            this.apellidoPText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.apellidoPText.Location = new System.Drawing.Point(550, 156);
            this.apellidoPText.Name = "apellidoPText";
            this.apellidoPText.Size = new System.Drawing.Size(231, 31);
            this.apellidoPText.TabIndex = 25;
            // 
            // apellidoPTitle
            // 
            this.apellidoPTitle.AutoSize = true;
            this.apellidoPTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.apellidoPTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.apellidoPTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.apellidoPTitle.Location = new System.Drawing.Point(369, 159);
            this.apellidoPTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.apellidoPTitle.Name = "apellidoPTitle";
            this.apellidoPTitle.Size = new System.Drawing.Size(167, 22);
            this.apellidoPTitle.TabIndex = 24;
            this.apellidoPTitle.Text = "Apellido Paterno:";
            this.apellidoPTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // apellidoMText
            // 
            this.apellidoMText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.apellidoMText.Location = new System.Drawing.Point(550, 211);
            this.apellidoMText.Name = "apellidoMText";
            this.apellidoMText.Size = new System.Drawing.Size(231, 31);
            this.apellidoMText.TabIndex = 27;
            // 
            // apellidoMTitle
            // 
            this.apellidoMTitle.AutoSize = true;
            this.apellidoMTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.apellidoMTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.apellidoMTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.apellidoMTitle.Location = new System.Drawing.Point(369, 214);
            this.apellidoMTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.apellidoMTitle.Name = "apellidoMTitle";
            this.apellidoMTitle.Size = new System.Drawing.Size(173, 22);
            this.apellidoMTitle.TabIndex = 26;
            this.apellidoMTitle.Text = "Apellido Materno:";
            this.apellidoMTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label1.Location = new System.Drawing.Point(20, 264);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(276, 23);
            this.label1.TabIndex = 28;
            this.label1.Text = "Fecha y lugar de nacimiento";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // mesText
            // 
            this.mesText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mesText.Location = new System.Drawing.Point(80, 351);
            this.mesText.Name = "mesText";
            this.mesText.Size = new System.Drawing.Size(208, 31);
            this.mesText.TabIndex = 32;
            // 
            // mesTitle
            // 
            this.mesTitle.AutoSize = true;
            this.mesTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.mesTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mesTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.mesTitle.Location = new System.Drawing.Point(20, 357);
            this.mesTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.mesTitle.Name = "mesTitle";
            this.mesTitle.Size = new System.Drawing.Size(51, 22);
            this.mesTitle.TabIndex = 31;
            this.mesTitle.Text = "Mes:";
            this.mesTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // diaText
            // 
            this.diaText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.diaText.Location = new System.Drawing.Point(80, 299);
            this.diaText.Name = "diaText";
            this.diaText.Size = new System.Drawing.Size(208, 31);
            this.diaText.TabIndex = 30;
            this.diaText.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // diaTitle
            // 
            this.diaTitle.AutoSize = true;
            this.diaTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.diaTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.diaTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.diaTitle.Location = new System.Drawing.Point(20, 302);
            this.diaTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.diaTitle.Name = "diaTitle";
            this.diaTitle.Size = new System.Drawing.Size(45, 22);
            this.diaTitle.TabIndex = 29;
            this.diaTitle.Text = "Día:";
            this.diaTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // estadoNText
            // 
            this.estadoNText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.estadoNText.Location = new System.Drawing.Point(777, 299);
            this.estadoNText.Name = "estadoNText";
            this.estadoNText.Size = new System.Drawing.Size(208, 31);
            this.estadoNText.TabIndex = 36;
            // 
            // estadoNTitle
            // 
            this.estadoNTitle.AutoSize = true;
            this.estadoNTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.estadoNTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.estadoNTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.estadoNTitle.Location = new System.Drawing.Point(626, 302);
            this.estadoNTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.estadoNTitle.Name = "estadoNTitle";
            this.estadoNTitle.Size = new System.Drawing.Size(77, 22);
            this.estadoNTitle.TabIndex = 35;
            this.estadoNTitle.Text = "Estado:";
            this.estadoNTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // paisNText
            // 
            this.paisNText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.paisNText.Location = new System.Drawing.Point(381, 351);
            this.paisNText.Name = "paisNText";
            this.paisNText.Size = new System.Drawing.Size(223, 31);
            this.paisNText.TabIndex = 34;
            // 
            // paisNTitle
            // 
            this.paisNTitle.AutoSize = true;
            this.paisNTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.paisNTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.paisNTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.paisNTitle.Location = new System.Drawing.Point(313, 354);
            this.paisNTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.paisNTitle.Name = "paisNTitle";
            this.paisNTitle.Size = new System.Drawing.Size(49, 22);
            this.paisNTitle.TabIndex = 33;
            this.paisNTitle.Text = "País:";
            this.paisNTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // anoText
            // 
            this.anoText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.anoText.Location = new System.Drawing.Point(381, 299);
            this.anoText.Name = "anoText";
            this.anoText.Size = new System.Drawing.Size(223, 31);
            this.anoText.TabIndex = 38;
            this.anoText.TextChanged += new System.EventHandler(this.textBox6_TextChanged);
            // 
            // anoTitle
            // 
            this.anoTitle.AutoSize = true;
            this.anoTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.anoTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.anoTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.anoTitle.Location = new System.Drawing.Point(313, 302);
            this.anoTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.anoTitle.Name = "anoTitle";
            this.anoTitle.Size = new System.Drawing.Size(54, 22);
            this.anoTitle.TabIndex = 37;
            this.anoTitle.Text = "Año:";
            this.anoTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.anoTitle.Click += new System.EventHandler(this.label7_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label6.Location = new System.Drawing.Point(20, 115);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(189, 23);
            this.label6.TabIndex = 39;
            this.label6.Text = "Información básica";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // rfcText
            // 
            this.rfcText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rfcText.Location = new System.Drawing.Point(888, 156);
            this.rfcText.Name = "rfcText";
            this.rfcText.Size = new System.Drawing.Size(208, 31);
            this.rfcText.TabIndex = 41;
            // 
            // rfcTitle
            // 
            this.rfcTitle.AutoSize = true;
            this.rfcTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rfcTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rfcTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.rfcTitle.Location = new System.Drawing.Point(814, 159);
            this.rfcTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.rfcTitle.Name = "rfcTitle";
            this.rfcTitle.Size = new System.Drawing.Size(51, 22);
            this.rfcTitle.TabIndex = 40;
            this.rfcTitle.Text = "RFC:";
            this.rfcTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cpText
            // 
            this.cpText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cpText.Location = new System.Drawing.Point(1011, 494);
            this.cpText.Name = "cpText";
            this.cpText.Size = new System.Drawing.Size(107, 31);
            this.cpText.TabIndex = 52;
            // 
            // cpTitle
            // 
            this.cpTitle.AutoSize = true;
            this.cpTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cpTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cpTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.cpTitle.Location = new System.Drawing.Point(955, 500);
            this.cpTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.cpTitle.Name = "cpTitle";
            this.cpTitle.Size = new System.Drawing.Size(41, 22);
            this.cpTitle.TabIndex = 51;
            this.cpTitle.Text = "CP:";
            this.cpTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cpTitle.Click += new System.EventHandler(this.label2_Click);
            // 
            // estadoDText
            // 
            this.estadoDText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.estadoDText.Location = new System.Drawing.Point(680, 494);
            this.estadoDText.Name = "estadoDText";
            this.estadoDText.Size = new System.Drawing.Size(244, 31);
            this.estadoDText.TabIndex = 50;
            // 
            // estadoDTitle
            // 
            this.estadoDTitle.AutoSize = true;
            this.estadoDTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.estadoDTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.estadoDTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.estadoDTitle.Location = new System.Drawing.Point(594, 500);
            this.estadoDTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.estadoDTitle.Name = "estadoDTitle";
            this.estadoDTitle.Size = new System.Drawing.Size(77, 22);
            this.estadoDTitle.TabIndex = 49;
            this.estadoDTitle.Text = "Estado:";
            this.estadoDTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // paisDText
            // 
            this.paisDText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.paisDText.Location = new System.Drawing.Point(840, 442);
            this.paisDText.Name = "paisDText";
            this.paisDText.Size = new System.Drawing.Size(230, 31);
            this.paisDText.TabIndex = 48;
            // 
            // paisDTitle
            // 
            this.paisDTitle.AutoSize = true;
            this.paisDTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.paisDTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.paisDTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.paisDTitle.Location = new System.Drawing.Point(771, 445);
            this.paisDTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.paisDTitle.Name = "paisDTitle";
            this.paisDTitle.Size = new System.Drawing.Size(49, 22);
            this.paisDTitle.TabIndex = 47;
            this.paisDTitle.Text = "País:";
            this.paisDTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // numeroText
            // 
            this.numeroText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numeroText.Location = new System.Drawing.Point(118, 494);
            this.numeroText.Name = "numeroText";
            this.numeroText.Size = new System.Drawing.Size(108, 31);
            this.numeroText.TabIndex = 46;
            // 
            // numeroTitle
            // 
            this.numeroTitle.AutoSize = true;
            this.numeroTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.numeroTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numeroTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.numeroTitle.Location = new System.Drawing.Point(20, 500);
            this.numeroTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.numeroTitle.Name = "numeroTitle";
            this.numeroTitle.Size = new System.Drawing.Size(88, 22);
            this.numeroTitle.TabIndex = 45;
            this.numeroTitle.Text = "Número:";
            this.numeroTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.numeroTitle.Click += new System.EventHandler(this.label5_Click);
            // 
            // calleText
            // 
            this.calleText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calleText.Location = new System.Drawing.Point(118, 442);
            this.calleText.Name = "calleText";
            this.calleText.Size = new System.Drawing.Size(258, 31);
            this.calleText.TabIndex = 44;
            // 
            // calleTitle
            // 
            this.calleTitle.AutoSize = true;
            this.calleTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.calleTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calleTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.calleTitle.Location = new System.Drawing.Point(20, 445);
            this.calleTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.calleTitle.Name = "calleTitle";
            this.calleTitle.Size = new System.Drawing.Size(61, 22);
            this.calleTitle.TabIndex = 43;
            this.calleTitle.Text = "Calle:";
            this.calleTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // direccionTitle
            // 
            this.direccionTitle.AutoSize = true;
            this.direccionTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.direccionTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.direccionTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.direccionTitle.Location = new System.Drawing.Point(20, 407);
            this.direccionTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.direccionTitle.Name = "direccionTitle";
            this.direccionTitle.Size = new System.Drawing.Size(98, 23);
            this.direccionTitle.TabIndex = 42;
            this.direccionTitle.Text = "Dirección";
            this.direccionTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.direccionTitle.Click += new System.EventHandler(this.label8_Click);
            // 
            // coloniaText
            // 
            this.coloniaText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.coloniaText.Location = new System.Drawing.Point(332, 494);
            this.coloniaText.Name = "coloniaText";
            this.coloniaText.Size = new System.Drawing.Size(244, 31);
            this.coloniaText.TabIndex = 54;
            // 
            // coloniaTitle
            // 
            this.coloniaTitle.AutoSize = true;
            this.coloniaTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.coloniaTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.coloniaTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.coloniaTitle.Location = new System.Drawing.Point(242, 500);
            this.coloniaTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.coloniaTitle.Name = "coloniaTitle";
            this.coloniaTitle.Size = new System.Drawing.Size(85, 22);
            this.coloniaTitle.TabIndex = 53;
            this.coloniaTitle.Text = "Colonia:";
            this.coloniaTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ciudadText
            // 
            this.ciudadText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ciudadText.Location = new System.Drawing.Point(508, 445);
            this.ciudadText.Name = "ciudadText";
            this.ciudadText.Size = new System.Drawing.Size(243, 31);
            this.ciudadText.TabIndex = 56;
            // 
            // ciudadTitle
            // 
            this.ciudadTitle.AutoSize = true;
            this.ciudadTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ciudadTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ciudadTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ciudadTitle.Location = new System.Drawing.Point(395, 448);
            this.ciudadTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.ciudadTitle.Name = "ciudadTitle";
            this.ciudadTitle.Size = new System.Drawing.Size(84, 22);
            this.ciudadTitle.TabIndex = 55;
            this.ciudadTitle.Text = "Ciudad:";
            this.ciudadTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // datosTitle
            // 
            this.datosTitle.AutoSize = true;
            this.datosTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.datosTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.datosTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.datosTitle.Location = new System.Drawing.Point(20, 553);
            this.datosTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.datosTitle.Name = "datosTitle";
            this.datosTitle.Size = new System.Drawing.Size(141, 23);
            this.datosTitle.TabIndex = 57;
            this.datosTitle.Text = "Datos profesor";
            this.datosTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // categoriaTitle
            // 
            this.categoriaTitle.AutoSize = true;
            this.categoriaTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.categoriaTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.categoriaTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.categoriaTitle.Location = new System.Drawing.Point(515, 598);
            this.categoriaTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.categoriaTitle.Name = "categoriaTitle";
            this.categoriaTitle.Size = new System.Drawing.Size(109, 22);
            this.categoriaTitle.TabIndex = 61;
            this.categoriaTitle.Text = "Categoria:";
            this.categoriaTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // categoriaText
            // 
            this.categoriaText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.categoriaText.FormattingEnabled = true;
            this.categoriaText.Items.AddRange(new object[] {
            "Planta",
            "Cátedra"});
            this.categoriaText.Location = new System.Drawing.Point(649, 595);
            this.categoriaText.Name = "categoriaText";
            this.categoriaText.Size = new System.Drawing.Size(216, 30);
            this.categoriaText.TabIndex = 62;
            this.categoriaText.SelectedIndexChanged += new System.EventHandler(this.categoriaText_SelectedIndexChanged);
            // 
            // maestriaTitle
            // 
            this.maestriaTitle.AutoSize = true;
            this.maestriaTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.maestriaTitle.Location = new System.Drawing.Point(24, 597);
            this.maestriaTitle.Name = "maestriaTitle";
            this.maestriaTitle.Size = new System.Drawing.Size(107, 26);
            this.maestriaTitle.TabIndex = 63;
            this.maestriaTitle.Text = "Maestría";
            this.maestriaTitle.UseVisualStyleBackColor = true;
            this.maestriaTitle.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // doctoradoTitle
            // 
            this.doctoradoTitle.AutoSize = true;
            this.doctoradoTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.doctoradoTitle.Location = new System.Drawing.Point(24, 652);
            this.doctoradoTitle.Name = "doctoradoTitle";
            this.doctoradoTitle.Size = new System.Drawing.Size(130, 26);
            this.doctoradoTitle.TabIndex = 64;
            this.doctoradoTitle.Text = "Doctorado";
            this.doctoradoTitle.UseVisualStyleBackColor = true;
            this.doctoradoTitle.CheckedChanged += new System.EventHandler(this.doctoradoTitle_CheckedChanged);
            // 
            // maestriaText
            // 
            this.maestriaText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.maestriaText.Location = new System.Drawing.Point(160, 597);
            this.maestriaText.Name = "maestriaText";
            this.maestriaText.Size = new System.Drawing.Size(333, 31);
            this.maestriaText.TabIndex = 65;
            this.maestriaText.Visible = false;
            // 
            // doctoradoText
            // 
            this.doctoradoText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.doctoradoText.Location = new System.Drawing.Point(160, 652);
            this.doctoradoText.Name = "doctoradoText";
            this.doctoradoText.Size = new System.Drawing.Size(333, 31);
            this.doctoradoText.TabIndex = 66;
            this.doctoradoText.Visible = false;
            // 
            // cipTitle
            // 
            this.cipTitle.AutoSize = true;
            this.cipTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cipTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cipTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.cipTitle.Location = new System.Drawing.Point(515, 656);
            this.cipTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.cipTitle.Name = "cipTitle";
            this.cipTitle.Size = new System.Drawing.Size(46, 22);
            this.cipTitle.TabIndex = 67;
            this.cipTitle.Text = "CIP:";
            this.cipTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label2.Location = new System.Drawing.Point(184, 558);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(415, 19);
            this.label2.TabIndex = 69;
            this.label2.Text = "* Separar múltiples doctorados, maestrías y correos con \",\" *";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label2.Click += new System.EventHandler(this.label2_Click_1);
            // 
            // regresarBoton
            // 
            this.regresarBoton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(109)))), ((int)(((byte)(188)))));
            this.regresarBoton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.regresarBoton.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.regresarBoton.ForeColor = System.Drawing.Color.White;
            this.regresarBoton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.regresarBoton.Location = new System.Drawing.Point(948, 676);
            this.regresarBoton.Name = "regresarBoton";
            this.regresarBoton.Size = new System.Drawing.Size(148, 41);
            this.regresarBoton.TabIndex = 70;
            this.regresarBoton.Text = "Regresar";
            this.regresarBoton.UseVisualStyleBackColor = false;
            this.regresarBoton.Click += new System.EventHandler(this.regresarBoton_Click);
            // 
            // cipText
            // 
            this.cipText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cipText.FormattingEnabled = true;
            this.cipText.Location = new System.Drawing.Point(649, 653);
            this.cipText.Name = "cipText";
            this.cipText.Size = new System.Drawing.Size(216, 30);
            this.cipText.TabIndex = 71;
            // 
            // nacionalidadText
            // 
            this.nacionalidadText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nacionalidadText.Location = new System.Drawing.Point(777, 354);
            this.nacionalidadText.Name = "nacionalidadText";
            this.nacionalidadText.Size = new System.Drawing.Size(223, 31);
            this.nacionalidadText.TabIndex = 73;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label3.Location = new System.Drawing.Point(626, 357);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(139, 22);
            this.label3.TabIndex = 72;
            this.label3.Text = "Nacionalidad:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // emailTitle
            // 
            this.emailTitle.AutoSize = true;
            this.emailTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.emailTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.emailTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.emailTitle.Location = new System.Drawing.Point(20, 710);
            this.emailTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.emailTitle.Name = "emailTitle";
            this.emailTitle.Size = new System.Drawing.Size(67, 22);
            this.emailTitle.TabIndex = 76;
            this.emailTitle.Text = "E-mail:";
            this.emailTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // telefonoTitle
            // 
            this.telefonoTitle.AutoSize = true;
            this.telefonoTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.telefonoTitle.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.telefonoTitle.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.telefonoTitle.Location = new System.Drawing.Point(515, 710);
            this.telefonoTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.telefonoTitle.Name = "telefonoTitle";
            this.telefonoTitle.Size = new System.Drawing.Size(92, 22);
            this.telefonoTitle.TabIndex = 74;
            this.telefonoTitle.Text = "Teléfono:";
            this.telefonoTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // emailText
            // 
            this.emailText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.emailText.Location = new System.Drawing.Point(160, 707);
            this.emailText.Name = "emailText";
            this.emailText.Size = new System.Drawing.Size(333, 31);
            this.emailText.TabIndex = 77;
            // 
            // telefonoText
            // 
            this.telefonoText.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.telefonoText.Location = new System.Drawing.Point(649, 701);
            this.telefonoText.Name = "telefonoText";
            this.telefonoText.Size = new System.Drawing.Size(216, 31);
            this.telefonoText.TabIndex = 78;
            // 
            // AltaProfesor
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1159, 778);
            this.Controls.Add(this.telefonoText);
            this.Controls.Add(this.emailText);
            this.Controls.Add(this.emailTitle);
            this.Controls.Add(this.telefonoTitle);
            this.Controls.Add(this.nacionalidadText);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cipText);
            this.Controls.Add(this.regresarBoton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cipTitle);
            this.Controls.Add(this.doctoradoText);
            this.Controls.Add(this.maestriaText);
            this.Controls.Add(this.doctoradoTitle);
            this.Controls.Add(this.maestriaTitle);
            this.Controls.Add(this.categoriaText);
            this.Controls.Add(this.categoriaTitle);
            this.Controls.Add(this.datosTitle);
            this.Controls.Add(this.ciudadText);
            this.Controls.Add(this.ciudadTitle);
            this.Controls.Add(this.coloniaText);
            this.Controls.Add(this.coloniaTitle);
            this.Controls.Add(this.cpText);
            this.Controls.Add(this.cpTitle);
            this.Controls.Add(this.estadoDText);
            this.Controls.Add(this.estadoDTitle);
            this.Controls.Add(this.paisDText);
            this.Controls.Add(this.paisDTitle);
            this.Controls.Add(this.numeroText);
            this.Controls.Add(this.numeroTitle);
            this.Controls.Add(this.calleText);
            this.Controls.Add(this.calleTitle);
            this.Controls.Add(this.direccionTitle);
            this.Controls.Add(this.rfcText);
            this.Controls.Add(this.rfcTitle);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.anoText);
            this.Controls.Add(this.anoTitle);
            this.Controls.Add(this.estadoNText);
            this.Controls.Add(this.estadoNTitle);
            this.Controls.Add(this.paisNText);
            this.Controls.Add(this.paisNTitle);
            this.Controls.Add(this.mesText);
            this.Controls.Add(this.mesTitle);
            this.Controls.Add(this.diaText);
            this.Controls.Add(this.diaTitle);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.apellidoMText);
            this.Controls.Add(this.apellidoMTitle);
            this.Controls.Add(this.apellidoPText);
            this.Controls.Add(this.apellidoPTitle);
            this.Controls.Add(this.nombreText);
            this.Controls.Add(this.nombreTitle);
            this.Controls.Add(this.nominaText);
            this.Controls.Add(this.nominaTitle);
            this.Controls.Add(this.LaberQuerie);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.ejecutarBoton);
            this.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AltaProfesor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LaberQuerie;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button ejecutarBoton;
        private System.Windows.Forms.Label nominaTitle;
        private System.Windows.Forms.TextBox nominaText;
        private System.Windows.Forms.TextBox nombreText;
        private System.Windows.Forms.Label nombreTitle;
        private System.Windows.Forms.TextBox apellidoPText;
        private System.Windows.Forms.Label apellidoPTitle;
        private System.Windows.Forms.TextBox apellidoMText;
        private System.Windows.Forms.Label apellidoMTitle;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox mesText;
        private System.Windows.Forms.Label mesTitle;
        private System.Windows.Forms.TextBox diaText;
        private System.Windows.Forms.Label diaTitle;
        private System.Windows.Forms.TextBox estadoNText;
        private System.Windows.Forms.Label estadoNTitle;
        private System.Windows.Forms.TextBox paisNText;
        private System.Windows.Forms.Label paisNTitle;
        private System.Windows.Forms.TextBox anoText;
        private System.Windows.Forms.Label anoTitle;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox rfcText;
        private System.Windows.Forms.Label rfcTitle;
        private System.Windows.Forms.TextBox cpText;
        private System.Windows.Forms.Label cpTitle;
        private System.Windows.Forms.TextBox estadoDText;
        private System.Windows.Forms.Label estadoDTitle;
        private System.Windows.Forms.TextBox paisDText;
        private System.Windows.Forms.Label paisDTitle;
        private System.Windows.Forms.TextBox numeroText;
        private System.Windows.Forms.Label numeroTitle;
        private System.Windows.Forms.TextBox calleText;
        private System.Windows.Forms.Label calleTitle;
        private System.Windows.Forms.Label direccionTitle;
        private System.Windows.Forms.TextBox coloniaText;
        private System.Windows.Forms.Label coloniaTitle;
        private System.Windows.Forms.TextBox ciudadText;
        private System.Windows.Forms.Label ciudadTitle;
        private System.Windows.Forms.Label datosTitle;
        private System.Windows.Forms.Label categoriaTitle;
        private System.Windows.Forms.ComboBox categoriaText;
        private System.Windows.Forms.CheckBox maestriaTitle;
        private System.Windows.Forms.CheckBox doctoradoTitle;
        private System.Windows.Forms.TextBox maestriaText;
        private System.Windows.Forms.TextBox doctoradoText;
        private System.Windows.Forms.Label cipTitle;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button regresarBoton;
        private System.Windows.Forms.ComboBox cipText;
        private System.Windows.Forms.TextBox nacionalidadText;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label emailTitle;
        private System.Windows.Forms.Label telefonoTitle;
        private System.Windows.Forms.TextBox emailText;
        private System.Windows.Forms.TextBox telefonoText;
    }
}